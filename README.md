#Deep Models for Drug Synergy Prediction in Cancer

We introduce an automatic learning framework leveraging cascade deep neural networks for drug synergy prediction. All the parameters in the proposed network including preprocessing of the input data, prior knowledge integration, and architecture of the neural networks and their internal parameters were selected using hyper-parameter optimization of the regression cost on drug synergy scores. The top performing cross-validated models were submitted for scoring on the challenge leaderboard.

Full documentation can be found here:

[https://www.synapse.org/#!Synapse:syn5700466/wiki/394537](https://www.synapse.org/#!Synapse:syn5700466/wiki/394537)