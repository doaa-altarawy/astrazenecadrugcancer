import pandas as pd
from pubchempy import get_compounds, Compound
import numpy as np
import binascii

def byte_to_binary(n):
    return ''.join(str((n & (1 << i)) and 1) for i in reversed(range(8)))

def hex_to_binary(h):
    return ''.join(byte_to_binary(ord(b)) for b in binascii.unhexlify(h))

def load_drug_info(filename):
    df = pd.read_csv(filename)

    return df

def save_drug_file(df, filename):
    df.to_csv(filename)
    return


def add_fingerprint(df):
    completed_df = df.copy()
    # 881 bit for the fingerprint
    for i in range(881):
        c = 'finger'+ str(i)
        completed_df[c] = 0
    print 'after adding columns', completed_df.shape
    for i, row in df.iterrows():
        smile = row['SMILES']
        if not pd.isnull(smile):
            print smile
            smile= str(smile).strip()
            try:
                comps = get_compounds(smile, 'smiles')
                # comps = get_compounds(smile, 'smiles', as_dataframe=True)
                fp = comps[0].fingerprint
                binary_string = hex_to_binary(fp)
                # first 4 bytes are for the fingerprint length
                # the last 7 bits are for padding
                binary_string = binary_string[32:-7]
                print len(binary_string)
                for j, b in enumerate(binary_string):
                    col = 'finger'+ str(j)
                    # print col
                    completed_df.loc[i, col] = b

            except:
                print 'error'
    return completed_df


def complete_info_using_pubchemid(df):
    completed_df = df.copy()

    for i, row in df.iterrows():
        id = row['PubChemID']
        # print type(id)
        if not pd.isnull(id):
            print id
            comp = Compound.from_cid(int(id))
            completed_df.loc[i, 'SMILES'] = comp.canonical_smiles
            completed_df.loc[i, 'MW'] = comp.molecular_weight
            completed_df.loc[i, 'HBD'] = comp.h_bond_donor_count
            completed_df.loc[i, 'HBA'] = comp.h_bond_acceptor_count
            completed_df.loc[i, 'cLogP'] = comp.xlogp
            # completed_df.loc[i, 'SMILES'] = 'SMILES'


            # print comp.isomeric_smiles
            # print comp.molecular_weight
            # print comp.h_bond_donor_count
            # print comp.h_bond_acceptor_count
            #
            # print repr(comp.fingerprint)
    return completed_df