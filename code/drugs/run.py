from process_drug_info import *
filename = '/Users/marakeby/PycharmProjects/workspace/astrazenecadrug_round4/data/Drug_synergy_data/drugs_info/Drug_info_release_2columns.csv'

info = load_drug_info(filename)
print info.head()
print info.shape

info = complete_info_using_pubchemid(info)
info = add_fingerprint(info)
save_drug_file(info, 'drug_info.csv')