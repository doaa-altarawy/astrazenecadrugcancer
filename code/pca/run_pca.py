import os
from sklearn import preprocessing as p
from sklearn.decomposition import PCA
from dataLoader.data_factory_clean import get_data
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd


data_params_no_filter = {
                'subchallenge': 'B',
                'load_meyth': False,
                'meyth_aggr': 'beta_corr_tissue',  # 'beta_sum',
                'load_ge': False,
                'load_cnv': False,
                'load_mut': False,
                'ge_impute': True,
                'ge_auto': False,
                'mut_auto': False,
                'cnv_auto': False,
                'meth_auto': False,
                'ge_pca': False,
                'meth_pca': False,
                'mut_pca': False,
                'cnv_pca': False,
                'mut_agg_type': 1,
                'cnv_agg_type': 0,
                'mut_filter': None,
                'cnv_filter': None,
                'auto_path': None,
                'pca_path': None,
                'prior_integration': None,
                'scaling_factor': 100,
                'pathways': None
}

data_params_full_drug_pathways = {
                'subchallenge': 'B',
                'load_meyth': False,
                'meyth_aggr': 'beta_corr_tissue',
                'load_ge': False,
                'load_cnv': False,
                'load_mut': False,
                'ge_impute': True,
                'ge_auto': False,
                'mut_auto': False,
                'cnv_auto': False,
                'meth_auto': False,
                'ge_pca': False,
                'meth_pca': False,
                'mut_pca': False,
                'cnv_pca': False,
                'mut_agg_type': 1,
                'cnv_agg_type': 0,
                'mut_filter': None,
                'cnv_filter': None,
                'auto_path': None,
                'pca_path': None,
                'prior_integration': 'filtering',
                'scaling_factor': 100,
                'pathways': [
                    'pathways_cancer',
                    'minoacyl-tRNA',
                    'NF-kappaB',
                    'cell_cycle',
                    'transforming_growth_factor-beta',
                    'apoptosis',
                    'p53_signaling',
                    'MAPK_signaling',
                    'PI3K-Akt_signaling',
                    'Wnt_signaling',
                    'mTOR_signaling',
                    # 'misreulation_cancer' ##
                ]
}


def normalize(in_data):
    proc =  p.StandardScaler()
    print in_data
    in_data = proc.fit_transform(in_data)
    proc= p.MinMaxScaler(feature_range=(-1, 1),)
    in_data = proc.fit_transform(in_data)
    return in_data

def visualize(orig_data, transformed_data):
    m = np.mean(orig_data, axis=0)

    print orig_data.shape, type(orig_data)
    print transformed_data.shape
    plt.plot(m[0:100])
    m = np.mean(transformed_data, axis=0)
    plt.plot(m[0:100])
    plt.legend(['original','transformed'])
    plt.show()


# # no filtering-----------------
data_params  = data_params_no_filter
prefix = ''


data_params= data_params_no_filter
prefix = ''



dataTypes = np.array(['load_meyth', 'load_ge', 'load_cnv', 'load_mut'])


base_directory = '../../processed_data/pca/'

# random_seed= 8082008
# np.random.seed(random_seed)
# print('random seed')
# print(random_seed)


def generate_PCA(data_params=data_params, dataTypes=dataTypes, base_directory=base_directory):
    for i, data_type in enumerate(dataTypes):
        data_params [data_type] = True
        data = get_data(data_params)
        data_params [data_type] = False   # Make it False for next loop
        data.fillna(0, inplace=True)

        info = data.index
        print data.shape
        data = data.as_matrix()

        print("**********************************************")
        print("PCA for Data Type: ", data_type)

        pca = PCA(50)
        pca.fit(data)
        data_transformed = pca.transform(data)


        # Save extracted features/ constructed data
        n= data_transformed.shape[1]
        df = pd.DataFrame(data_transformed, index=info, columns=range(0,n))

        save_dir = os.path.join(base_directory,prefix )
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        file_name = data_type
        if data_type == 'load_meyth':
            file_name = 'meth_pca.csv'

        elif data_type == 'load_ge':
            file_name = 'ge_pca.csv'

        elif data_type == 'load_mut':
            file_name = 'mu_pca.csv'

        elif data_type == 'load_cnv':
            file_name = 'cnv_pca.csv'

        file_name = os.path.join(save_dir, file_name )
        df.to_csv( file_name)
