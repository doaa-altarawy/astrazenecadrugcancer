import keras
from sklearn.pipeline import Pipeline
import theano
theano.config.openmp = False
from keras.models import Sequential
from keras.layers.core import Dense, Activation, ActivityRegularization, AutoEncoder, Merge, Dropout
from keras.optimizers import SGD, rmsprop
from keras.regularizers import l1, l2, l1l2, activity_l2
import numpy as np
import logging
from preprocessing import pre, smart
from sklearn import preprocessing as p, clone
import os

from matplotlib import pyplot as plt
_plot= False
_plot_hist = False

class NN(object):

    def get_datatype_net(self,input_shape):

        model = Sequential()
        # l1-------
        # firstlayer= Dense(input_shape, input_shape=(input_shape,), init=self.init, W_constraint = OneToOne(), W_regularizer=l1(0.00001))
        # firstlayer= Dense(100, input_shape=(input_shape,), init=self.init)
        # firstlayer= Dense(100, input_dim=input_shape)
        # self.firstlayer= Dense(input_shape, input_shape=(input_shape,), init=self.init)
        #self.firstlayer= Dense(input_shape, input_dim=input_shape, init=self.init)


        for i, nhid in enumerate(self.dataype_nhids):

            if self.reg_type =='l2':
                w_reg = l2(self.datatype_regularization[i])
            else:
                w_reg = l1(self.datatype_regularization[i])

            if i ==0:
                firstlayer= Dense(nhid, input_dim=input_shape, activation=self.activation, W_regularizer = w_reg)
                model.add(firstlayer)
            else:
                layer= Dense(nhid,  activation=self.activation, W_regularizer = w_reg)
                model.add(layer)

            if not self.datatype_dropouts ==0:
                drop = self.datatype_dropouts[i]
                model.add(Dropout(drop))

        return model

    def get_list_architecture(self, x):
        data_types = set(x.columns.get_level_values(0))
        # for d in data_types:
            # print 'shape of', d, x[d].shape

        # print 'data_types', data_types
        x_list =[]
        for sets in self.architecture:
                sets = list(sets)
                # this is misleading, dont change the user inputs, it is better to throw an exception than running with wrong params.
                # If tissue_type is set to False, then it is not loaded, remove from NN
                # if ('tissue_type' not in x.columns):
                #     sets.remove('tissue_type')
                logging.info('shape of %s, %s',sets, x[sets].shape)
                t = x[sets].as_matrix()
                x_list.append(t )
        # print 'x_list', len(x_list)
        return x_list

    def get_list(self,x):
        return self.get_list_architecture( x)

    # def get_list(self, x):
    #     data_types = set(x.columns.get_level_values(0))
    #
    #     print 'data_types', data_types
    #     x_list =[]
    #     for d in data_types:
    #         if d == 'drugs' and self.account_for_drugs:
    #             continue
    #         if self.account_for_drugs:
    #             types= [d,'drugs']
    #         else:
    #             types = d
    #         print '%s shape '%d, x[types].shape
    #         t = x[types].as_matrix()
    #         x_list.append(t )
    #     return x_list

    def get_model(self, X_train):

        x_list  = self.get_list(X_train)
        models = []

        for x in x_list:
            n = x.shape[1]
            m = self.get_datatype_net(n)
            models.append(m)

        model = Sequential()
        model.add(Merge(models, mode=self.merger, concat_axis=1))

        for i, nhid in enumerate(self.more_nhids):
            if self.reg_type =='l2':
                w_reg = l2(self.more_regularization[i])
            else:
                w_reg = l1(self.more_regularization[i])

            layer= Dense(nhid, init= self.init, activation= self.activation, W_regularizer = w_reg)
            model.add(layer)
            if not self.more_dropouts ==0:
                drop = self.more_dropouts[i]
                model.add(Dropout(drop))


        model.add(Dense(1, init=self.init, activation= 'linear'))

        sgd = SGD(lr= self.lr , decay= self.decay , momentum= self.momentum, nesterov=True)


        if self.rmsp:
            logging.info('rmsprop optimizer is used')
            rms = rmsprop(self.lr)
            model.compile(loss=self.loss, optimizer=rms)
        else:
            logging.info('sgd optimizer is used')
            model.compile(loss=self.loss, optimizer=sgd)

        yaml_string = model.to_yaml()

        return model


    def __init__(self, lr= 0.05, epoch=3, batch_size= 10, momentum=.9, activation = 'tanh', decay=1e-6,
                dataype_nhids= [100, 50, 20] ,
                 datatype_dropouts=0 ,
                 datatype_regularization = [0.001]*3,

                 more_nhids=[100],
                 merger ='concat',
                 more_dropouts=0,
                 more_regularization = [0.001],

                 rmsp=True, account_for_drugs =False,
                 architecture =[['cnv_AE', 'drugs'], ['mut_AE', 'drugs']],
                 preprocessors = [], reg_type = 'l2', verbose = 0,
                 early_stopping=None,
                 saveModels=None
                 ):

        processors = []
        for processor in preprocessors:
            proc = pre.get_processor(processor)
            processor_name = processor['type']
            processors.append((processor_name, proc))
        # print preprocessors

        if len(preprocessors) > 0:
            self.processors = Pipeline(processors)
        else:
            print 'no preprocessing applied to NN inputs'
            self.processors = None


        # self.proc1 = p.StandardScaler()
        # self.proc1 = smart.SmartPreprocesor(self.proc1)
        # self.proc2 = p.MinMaxScaler((-1,1))
        # self.proc2 = smart.SmartPreprocesor(self.proc2)

        # params
        self.lr = lr
        self.momentum = momentum
        self.decay = decay
        self.activation = activation #'relu', tanh, sigmoid
        self.nb_epoch = epoch
        self.batch_size= batch_size
        self.rmsp = rmsp
        # print self.activation
        self.dataype_nhids = dataype_nhids
        self.merger = merger
        self.more_nhids= more_nhids

        self.more_dropouts =more_dropouts
        self.datatype_dropouts = datatype_dropouts
        self.more_regularization = more_regularization
        self.datatype_regularization = datatype_regularization
        self.account_for_drugs = account_for_drugs
        self.architecture = list(architecture)
        # print self.architecture
        self.reg_type = reg_type
        self.verbose = verbose
        self.early_stopping = early_stopping
        self.saveModels = saveModels


        self.init = 'glorot_uniform'
        self.loss= 'mean_squared_error' # mean_absolute_error, mean_squared_logarithmic_error, mean_squared_error
        # self.loss= 'mean_squared_error' # mean_absolute_error, mean_squared_logarithmic_error, mean_squared_error
        # self.loss= 'mean_squared_error' # mean_absolute_error, mean_squared_logarithmic_error, mean_squared_error

    def set_params(self, **params):
        # print 'set_params called'
        print params
        pass

    def get_params(self, deep=True):
        params ={'lr': self.lr,
        'momentum': self.momentum,
        'decay': self.decay,
        'activation' : self.activation,#'relu', tanh, sigmoid:
        'epoch': self.nb_epoch,
        'batch_size': self.batch_size,
        'rmsp': self.rmsp ,

        'dataype_nhids': self.dataype_nhids,
        'merger': self.merger ,
        'more_nhids': self.more_nhids,

        'more_dropouts' : self.more_dropouts,
        'datatype_dropouts': self.datatype_dropouts,
        'more_regularization': self.more_regularization ,
        'datatype_regularization': self.datatype_regularization,
        'account_for_drugs': self.account_for_drugs,
        'architecture':  self.architecture ,
        'reg_type':  self.reg_type ,
        'verbose': self.verbose,
        'early_stopping': self.early_stopping ,
        'saveModels': self.saveModels }
        # 'glorot_uniform': self.init,
        # 'mean_squared_error':self.loss }
        return params

    def fit(self, X_train_in, y, X_val=None, y_val=None, model_file=None):
        print 'fitting nn'
        self.inner_model = self.get_model(X_train_in)
        if not model_file is None:
            self.load_weights(X_train_in, model_file)

        X_train = self.get_list(X_train_in)
        X_val = self.get_list(X_val)

        self.processoers1 = [None]*len(X_train)

        for i, data in enumerate(X_train):
            if not self.processors is None:
                p1 = clone(self.processors)
                # print 'applying preprocessing', p1
                data = p1.fit_transform(data)

                self.processoers1[i] = p1

            X_train[i] = data


        if not X_val is None:
            for i, data in enumerate(X_val):
                if not self.processors is None:
                    data = p1.fit_transform(data)
                X_val[i] = data

            if _plot:
                plt.plot(np.mean(data, axis=0))
        if _plot:
            plt.show()

        if not X_val is None:


            history = self.inner_model.fit(X_train, y, nb_epoch=self.nb_epoch, batch_size=self.batch_size, verbose= self.verbose,  validation_data= (X_val, y_val))
        else:
            history = self.inner_model.fit(X_train, y, nb_epoch=self.nb_epoch, batch_size=self.batch_size, verbose=self.verbose)

        # print history.history['loss']
        # print history.history['val_loss']

        if _plot_hist:
            loss= history.history['loss']
            val_loss = history.history['val_loss']

            plt.plot(loss)
            plt.plot(val_loss)
            plt.legend(['loss','val_loss'])
            plt.show()


        return self


    def predict(self, X_test):
        X_test = self.get_list(X_test)
        for i, data in enumerate(X_test) :
            print i, data.shape
            if not self.processors is None:
                # print 'applying preprocessing (testing)', self.processoers1[i]
                data = self.processoers1[i].transform(data)
            X_test[i] = data

        prediction = self.inner_model.predict(X_test)
        # print 'predictions,...', prediction.shape,  np.mean(prediction)
        return prediction


    def load_weights(self, X_train_in, filename=None):
        # Initialize a model
        self.inner_model = self.get_model(X_train_in)

        X_train = self.get_list(X_train_in)

        self.processoers1 = [None]*len(X_train)

        for i, data in enumerate(X_train):
            if not self.processors is None:
                p1 = clone(self.processors)
                # print 'applying preprocessing', p1
                data = p1.fit_transform(data)
                self.processoers1[i] = p1

            X_train[i] = data

        # Load saved weights
        self.inner_model.load_weights(filename)

