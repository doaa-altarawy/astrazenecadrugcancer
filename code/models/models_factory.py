__author__ = 'marakeby'

from models.multimodel import Multimodel
from sklearn import svm
from sklearn.linear_model import RidgeCV, ElasticNetCV, Ridge, Lasso, ElasticNet, LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import BaggingRegressor, RandomForestRegressor, AdaBoostRegressor

def removekey(d, key):
    r = dict(d)
    del r[key]
    return r

def get_model(params):
    model_type = params['type']
    p = params['params']
    print ('model type: ', model_type)
    print ('model paramters: ', p)
    # params = removekey(params, 'type')
    if model_type =='svm':
        # print params
        model =  svm.SVR(max_iter=1000 , **p)
    elif model_type =='ridge':
        model = Ridge(**p)
    elif model_type =='elastic':
        model = ElasticNet(**p)
    elif model_type =='lasso':
        model = Lasso(**p)
    elif model_type == 'randomforest':
        model = DecisionTreeRegressor(**p)

    elif model_type == 'AdaBoostDecisionTree':
        DT_params = params['DT_params']
        model =  AdaBoostRegressor(base_estimator=DecisionTreeRegressor(**DT_params), **p)
    elif model_type == 'RandomForestRegressor':
        model = RandomForestRegressor(**p)

    elif model_type =='multimodels':
        models_params = p['models']
        models = models_params.copy()
        for m in models_params:
            models[m]= get_model(models_params[m])

        model = Multimodel(models)

    elif model_type == 'nn_pipeline':
        from models import nn_pipeline
        model = nn_pipeline.NN(**p)
    elif model_type == 'nn_pipeline_custom':
        from models import nn_pipeline_custom
        print p
        if isinstance(p, tuple):
            p = dict(p[0].items() + p[1].items() + p[2].items()) # bug with hyperpramters definition
        model = nn_pipeline_custom.NN(**p)
    elif model_type == 'nn_auto_pipeline':
        from  models import model_auto_nn
        model = model_auto_nn.NN(**p)
    elif model_type == 'bioNN':
        from  models import bioNN
        model = bioNN.BioNN(**p)

    else:
        print ('wrong model type, default model will be used')
        model =  svm.SVR(C=0.1, epsilon=0.4, gamma=0.1)

    # if 'bag' in params:
    #     bag_params = params['bag']
    #     n_estimators = bag_params['n_estimators']
    #     n_jobs = bag_params['n_jobs']
    #     print ('bagging: n_estimators, n_jobs: ', n_estimators, n_jobs)
    #     print ('WARNING: bagging takes long time')
    #     if model_type == 'nn_pipeline_custom':
    #         model = Bag(model, n_estimators=n_estimators,n_jobs=n_jobs)
    #     else:
    #         model = BaggingRegressor(model, n_estimators=n_estimators,n_jobs=n_jobs)

    return model
