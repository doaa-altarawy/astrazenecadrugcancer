
import numpy as np
from sklearn.linear_model import RidgeCV, ElasticNetCV, Ridge, Lasso, ElasticNet, LinearRegression

_plot = True
from matplotlib import pyplot as plt



class Multimodel():

    def combine(self, pred):
        pred_matrix = self.get_pred_matrix(pred)
        # pred = pred_matrix.sum(axis=1)
        # print 'pred', pred_matrix.shape
        pred = self.weighted_combine(pred_matrix)
        # pred = pred

        return pred


    def get_pred_matrix(self ,pred):
        pred_list = []
        for k in pred:
            pred_list.append(pred[k])

        pred_matrix = np.column_stack(pred_list)
        return pred_matrix

    def weighted_combine(self, pred):
        # comb_model = Ridge(alpha=0.0)
        comb_model = LinearRegression(normalize=True)
        # p = {'splitter': 'random', 'max_depth': 10}
        # comb_model = DecisionTreeRegressor(**p)
        x_train = self.get_pred_matrix(self.pred_train)
        x_test = self.get_pred_matrix(self.pred_test)

        comb_model.fit(x_train, self.y_train)
        pred = comb_model.predict(x_test)
        return pred

    def __init__(self, models, combine='weighted'):

        self.models = models
        # for m in models:
        #     print m

    def fit(self, X_train, y):
        # X_train = adapt_data(X_train)
        y= np.ravel(y)
        self.y_train = y
        self.X_train = X_train
        data_types = set(X_train.columns.get_level_values(0)) # assume multiindex
        self.pred_train= {}
        for d in data_types:
            if d =='drugs' or d == 'tissue_type':
                continue
            print 'fitting model for:' + d

            if 'tissue_type' in data_types: # loaded in Subchallenge A only
                data_train = X_train[['drugs', 'tissue_type', d ]].as_matrix()
            else:
                data_train = X_train[['drugs', d ]].as_matrix()

            # print '%s shape '% d, data_train.shape
            model= self.models[d]
            model.fit(data_train, y)
            self.models[d] = model
            self.pred_train[d] = model.predict(data_train)
        return self


    def predict(self, X_test):
        data_types = set(X_test.columns.get_level_values(0)) # assume multiindex
        self.pred_test = {}
        for d in data_types:
            if d =='drugs' or d == 'tissue_type':
                continue
            if 'tissue_type' in data_types: # loaded in Subchallenge A only
                data_train = X_test[['drugs', 'tissue_type', d ]].as_matrix()
            else:
                data_train = X_test[['drugs', d ]].as_matrix()

            # print '%s shape '% d, data_train.shape
            model= self.models[d]
            self.pred_test[d] = model.predict(data_train)

        pred = self.combine(self.pred_test)
        return pred

