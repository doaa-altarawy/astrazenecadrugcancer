__author__ = 'haitham'

from dataLoader.data_low_level_clean import phase
from dataLoader.combin_priority import generate_combin_priority
from train_predict import run
import numpy as np
import datetime
import logging
import sys
import os

model_params = {
                'params': {
                    'verbose': 2,
                    'activation': 'sigmoid',
                    'saveModels': None,
                    'batch_size': 10,
                    'more_regularization': (0.01, 0.01, 0.01),
                    'more_nhids': (200, 100),
                    'early_stopping': None,
                    'datatype_regularization': (0.001, 0.001, 0.001),
                    'decay': 1e-06,
                    'merger': 'concat',
                    'datatype_dropouts': (0.3, 0.3, 0.3),
                    'dataype_nhids': (100,),
                    'epoch': 300, 'rmsp': True,
                    'lr': 0.001,
                    #  Tissue_type will be used ONLY if add_tissues param =True
                    'architecture': (('cnv_AE', 'drugs', 'tissue_type'), ('mut_AE', 'drugs', 'tissue_type'),
                                     ('drugs', 'tissue_type')),
                    'more_dropouts': (0.4, 0.4, 0.4),
                    'account_for_drugs': True,
                    'momentum': 0.9,
                    'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)
                },

                'type': 'nn_pipeline_custom'
            }



# --------------------  multimodel Random Forest -------------------------------/

# base_models_AE = {
#     'params': {'max_features': 'auto', 'splitter': 'best', 'max_depth': 70, 'criterion': 'friedman_mse'},
#     'type': 'randomforest',
#     'bag': {'n_estimators': 50, 'n_jobs': 3}
# }
#
# base_models_org = {
#     'params': {'max_features': 'auto', 'splitter': 'best', 'max_depth': 70, 'criterion': 'friedman_mse'},
#     'type': 'randomforest',
#     'bag': {'n_estimators': 50, 'n_jobs': 3}
# }
#
# models_sub1B = {'mu':base_models_org, 'cnv':base_models_org, 'mut_AE': base_models_AE, 'cnv_AE': base_models_AE}
# model_params_multi = {'type': 'multimodels', 'params': {'models': models_sub1B }}

# ---------------------------------------------------------------------------

pre_params = {'type': 'standard'}

data_params =  {
                'subchallenge': 'B', # 'A'
                'add_tissues': True,       # if False, no tissue info is used
                'drug_features': {
                            'subchallenge': 'B', # 'A'
                            'targets': True,
                            'drug_features': True,
                            'combination_factor':False,
                            'processed_targets': True,
                            'drug_info': False,
                            'log_scores': False,
                            'challenge':1,
                            'drug_auto' : None
                        },
                'pca_path' : 'pca/no_filter',
                # 'pca_path' : 'pca/full_drug_pathways'

                # 'auto_path': 'auto/full_drug_pathways/',
                'auto_path': 'auto/no_filter',



                'load_meyth': False, 'meth_auto' : False, 'meth_pca' : False,
                'meyth_aggr':  'beta_corr_tissue', # corr with ge, by tissue type #------> best
                                # 'beta_sum',     # no imputation, no corr, sum per gene
                'load_ge':  False, 'ge_auto':False, 'ge_pca' :False,
                'ge_impute': True,
                'load_cnv': False, 'cnv_auto':True, 'cnv_pca':False,
                'load_mut': False, 'mut_auto':True, 'mut_pca' : False,
                'mut_filter': None,
                'cnv_filter': None,

                'mut_agg_type': 1,  # 'raw': all possible features, numeric
                                    # 0: no aggregation, relevant features
                                    # 1: 1 feature per gene                # -----> Best
                'cnv_agg_type': 0,
                                    # 'raw: zygosity as categorical (6 features)
                                    # 0: zygosity as numeric (4 features)    #------> Best
                                    # 1: aggregation into one number
                'prior_integration': 'filtering',
                                     # None,  # no filtering of molecular data

                'scaling_factor':100,
                'pathways': [                       #------> Best
                                'pathways_cancer',
                                'minoacyl-tRNA',
                                'NF-kappaB',
                                'cell_cycle',
                                'transforming_growth_factor-beta',
                                'apoptosis',
                                'p53_signaling',
                                'MAPK_signaling',
                                'PI3K-Akt_signaling',
                                'Wnt_signaling',
                                'mTOR_signaling'
                              ],
                'tranform_by_DT':False,
               }


# setup logging
base_folder ='../logs'
timeStamp = '_{0:%b}-{0:%d}_{0:%H}-{0:%M}'.format(datetime.datetime.now())
filename= os.path.basename(__file__)
filename, file_extension = os.path.splitext(filename)
filename = os.path.join(base_folder,filename+ timeStamp+'.log')
print filename
logging.basicConfig(filename = filename,
                    filemode='w',
                    format='%(asctime)s - {%(filename)s:%(lineno)d} - %(message)s',
                    datefmt='%m/%d %I:%M',
                    level=logging.INFO) # or logging.DEBUG
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

details = 'sub1B final submission'
logging.info(details)
random_seed= 3003495
np.random.seed(random_seed)
logging.info('random seed')
logging.info(random_seed)

args = {}
args['data_params'] = data_params
args['model_params'] = model_params
args['pre_params'] = pre_params
logging.info("Args: %s", args)

#-------------------   generate submission  ----------------#

save_path = '../../results/sub1b/'
logging.info('Generating submission file in %s', save_path)

#---> Score leaderboard
from dataLoader.generate_processed_data import download_process_data

# AE_best = 8082008
# np.random.seed(AE_best)    # fix seed fo AE
# download_process_data()
AE_seeds = [8082008, 7667993, 530200, 130481]
LB_seeds = [8082008, 5878595, 7667993, 3373553, 6178984, 3795226, 2970918, 8516676, 3867199, 1878007]
number_of_processes = 4
for seed in AE_seeds:
    from functools import partial
    import multiprocessing
    tiebreaker_score_avg, final_score_avg = [], []
    # np.random.seed(seed) # fix seed fo AE
    # download_process_data()
    n_iters = 10

    seeds = LB_seeds[0:n_iters]
    save_path = '../results/BestRuns/test/'
    run_part = partial(run, data_phase=phase.training, save_path= save_path, args=args)
    pool = multiprocessing.Pool(number_of_processes)
    results= pool.map(run_part, seeds) # start running in parallel

    final_score_avg.append([-i[1] for i in results])
    tiebreaker_score_avg.append([-i[2] for i in results])

    logging.info('AE Seeeed: %s', seed)
    logging.info('Final scores: %s', final_score_avg)
    logging.info('Tie scores %s', tiebreaker_score_avg)
    logging.info('Avg Final scores: %s', np.mean(final_score_avg))
    logging.info('Avg Tie scores %s', np.mean(tiebreaker_score_avg))

#---> Generate submission for test data
# run(args, data_phase=phase.test, save_path= save_path)
#
# generate_combin_priority()
logging.info("VTForce2")
logging.info('Submission file saved in %s', save_path)
logging.info("Leaderboard scores: ")
# logging.info("Primary Metric: %s", -final_score)
# logging.info("Tie Breaking: %s", -tiebreaker_score)
logging.info('Tissue_type used=%s', data_params['add_tissues'])