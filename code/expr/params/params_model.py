
from hyperopt import hp
import numpy as np
#---------------------- svm ---------------
# http://scikit-learn.org/stable/auto_examples/svm/plot_rbf_parameters.html
# http://www.svms.org/parameters/
# http://www.csie.ntu.edu.tw/~cjlin/papers/guide/guide.pdf

# C: If it is too large, we have a high penalty for nonseparable points and we may store many support vectors and overfit. If it is too small, we may have underfitting."

C = hp.choice('svm_C',  np.logspace(-5, 5, 50))
epsilon=hp.choice('svm_rbf_epsilon', [0, 0.01, 0.1, 0.5, 1, 2, 4])
gamma =  hp.choice('svm_gamma',  np.logspace(-5, 5, 50))

svm = {
        'type': 'svm',
        'params': hp.pchoice('p',
                            [ (0.3, {'C': C, 'kernel': 'linear', }),
                              (0.7, {'C': C, 'kernel': 'rbf',  'epsilon': epsilon, 'gamma':gamma})]
                             ),
    }

#---------------------- ridge ---------------

alpha = hp.choice('alpha', np.logspace( -5, 3, 40))
ridge ={'type': 'ridge',
        'params':{'alpha': alpha},
        'bag': {'n_estimators': hp.choice('n_estimators_ridge',[1, 10, 20]), 'n_jobs': 2}}



#---------------------- elastic ---------------
elastic_alpha = hp.choice('elastic_alpha', np.logspace( -5, 3, 40))
l1_ratio = hp.uniform('l1_ratio', 0, 1)
elastic = {
     'type': 'elastic',
     'params': {'l1_ratio': l1_ratio, 'alpha': elastic_alpha},
        'bag': {'n_estimators': hp.choice('n_estimators_enet',[1, 10, 20]), 'n_jobs': 2}
     }



#---------------------- lasso ---------------
lasso_alpha = hp.choice('lasso_alpha', np.logspace(-5, 3, 40))
lasso = {
     'type': 'lasso',
     'params': {'alpha': lasso_alpha},
     'bag': {'n_estimators': hp.choice('n_estimators_lasso',[1, 10, 20]), 'n_jobs': 2}
     }


#-----------------------------------------------
features_perct = hp.uniform('max_uniform', 0, 1)
random_forest = {
        'type': 'randomforest',
        'params':{
            'max_depth': hp.choice('max_depth_RandomForest', range(40,100,10)),
            'max_features': 'auto', #hp.choice('max_features', [features_perct, 'log2', 'auto']),
            'splitter': 'best', #hp.choice('splitter', ['best', 'random'])
        },
    'bag': {
            'n_estimators': hp.choice('n_estimators_tree',[40, 50, 60, 70, 80, 90, 100]),
             'n_jobs': 2

    }
    }

#RandomForestRegressor = {
#        'type': 'RandomForestRegressor',
#        'params':{
#            'max_depth': hp.choice('max_depth2', range(5,15)),
#            'max_features': 'auto', #hp.choice('max_features2', [features_perct, 'log2', 'auto']),
#        }
#}

AdaBoostDecisionTree = {
       'type': 'AdaBoostDecisionTree',
       'params': {'learning_rate': 1, 'n_estimators': 50},
       'DT_params': {'max_features': 'auto',
                     'splitter': hp.choice('splitter', ['best', 'random']),
                     'max_depth': hp.choice('max_depth_AdaBoost', range(10, 101, 10)),
                     #'n_jobs': 3
                     }
}

#-------------------kernel estimator----------------------------
# alpha_kernel = hp.choice('alpha_kernel', [0.5 ,0.1, 0.001, 0.001])

# alpha_kernel = hp.choice('alpha_kernel', np.logspace(-5, 3, 40))
# sigma_kernel = hp.choice('sigma_kernel', [0.9, 0.5 ,0.4, 0.3, 0.1])

'''model_params = {
    'params': {'max_features': 'auto', 'splitter': 'random', 'max_depth': 50},
    'type': 'randomforest',
    'bag': {'n_estimators': 20, 'n_jobs': 1}
 }

gamma_rbf = hp.choice('gamma_rbf', np.logspace(-3, 3, 20))
gamma_laplace = hp.choice('gamma_laplace', np.logspace(-3, 3, 20))
gamma_ploy = hp.choice('gamma_ploy', np.logspace(-3, 3, 20))
coef_ploy = hp.choice('coef_ploy', np.logspace(-3, 3, 20))
degree_ploy = hp.choice('degree_ploy', [1, 2, 3, 4])

linear = {'type':'linear'}
rbf = {'type':'rbf', 'gamma': gamma_rbf}
# kernel = {'type':'chi2', 'gamma':4 }
poly = {'type':'poly', 'gamma': gamma_ploy, 'degree':degree_ploy, 'coef0':coef_ploy }
laplace = {'type':'laplacian', 'gamma': gamma_laplace}
cosine = {'type':'cosine'}

kernel_params = hp.choice('kernels', [rbf, linear,poly, laplace, cosine ])
kernel = {
    'params': {'kernel': kernel_params, 'base_model': model_params},
    'type': 'kernel_stimator'
}

'''

#-------------------multikernel estimator----------------------------


sigma_kernel_d = hp.choice('sigma_kernel_d', [0.9, 0.5 ,0.4, 0.3, 0.1])
sigma_kernel_ge = hp.choice('sigma_kernel_ge', [0.9, 0.5 ,0.4, 0.3, 0.1])
sigma_kernel_mu = hp.choice('sigma_kernel_mu', [0.9, 0.5 ,0.4, 0.3, 0.1])
sigma_kernel_cnv = hp.choice('sigma_kernel_cnv', [0.9, 0.5 ,0.4, 0.3, 0.1])
sigma_kernel_meth = hp.choice('sigma_kernel_meth', [0.9, 0.5 ,0.4, 0.3, 0.1])

#k_drugs= {'type': 'rbf', 'sigma':sigma_kernel_d}
#k_ge = {'type': 'rbf', 'sigma': sigma_kernel_ge}
#k_mu = {'type': 'rbf', 'sigma': sigma_kernel_mu}
#k_cnv = {'type': 'rbf', 'sigma': sigma_kernel_cnv}
#k_meth = {'type': 'rbf', 'sigma': sigma_kernel_meth}

k_drugs= {'type': 'rbf', 'sigma':0.1}
k_ge = {'type': 'rbf', 'sigma': 0.5}
k_mu = {'type': 'rbf', 'sigma': 0.5}
k_cnv = {'type': 'rbf', 'sigma': 0.3}
k_meth = {'type': 'rbf', 'sigma': sigma_kernel_meth}

#kernels = [k_drugs, k_ge]
kernels = [k_drugs, k_ge,  k_mu, k_cnv, k_meth]

#w_d = hp.choice('w_d', [1, 0.9, 0.5 ,0.4, 0.3, 0.1, 0.01])
#w_ge = hp.choice('w_ge', [1, 0.9, 0.5 ,0.4, 0.3, 0.1, 0.01])
#w_mu = hp.choice('w_mu', [1, 0.9, 0.5 ,0.4, 0.3, 0.1, 0.01])
#w_cnv = hp.choice('w_cnv', [1, 0.9, 0.5 ,0.4, 0.3, 0.1, 0.01])
#w_meth = hp.choice('w_meth', [1, 0.9, 0.5 ,0.4, 0.3, 0.1, 0.01])

w_d = hp.choice('w_d', np.logspace(-3, 3, 20))
w_ge = hp.choice('w_ge', np.logspace(-3, 3, 20))
w_mu = hp.choice('w_mu', np.logspace(-3, 3, 20))
w_cnv = hp.choice('w_cnv', np.logspace(-3, 3, 20))
w_meth = hp.choice('w_meth', np.logspace(-3, 3, 20))

weights= [w_d, w_ge, w_mu, w_cnv, w_meth]
multikernel= {'params': {'weights': weights, 'kernels': kernels}, 'type': 'multikernel'}

# ----------------- multi Models ------------------

#base_params = model_params
#models = {'ge': base_params, 'meth':base_params , 'mu':base_params, 'cnv':base_params}
#mutliModel_params = {'type': 'multimodels', 'params': {'models': models }}


# ----------------- nn_pipeline ------------------

#sgd only params
lr =  0.001
batch_size= 10
momentum=.9
decay=1e-6
#
dropouts= [ [0]*3, [0.1]*3, [0.2]*3, [0.3]*3]
reg = [[0]*3, [0.001]*3, [0.0001]*3]

epoch= hp.choice('epoch', [50, 100, 200])
activation =  hp.choice('activation', ['tanh', 'sigmoid'])
dataype_nhids= hp.choice('dataype_nhids', [[  100], [200, 100], [300, 200, 100], [100, 50]])
datatype_dropouts= hp.choice('datatype_dropouts',dropouts)
datatype_regularization = hp.choice('datatype_regularization',reg)

more_nhids= hp.choice('more_nhids', [[  100],[200], [200, 100], [300, 200, 100], [100, 50]])
more_dropouts = hp.choice('more_dropouts', dropouts)
more_regularization =  hp.choice('more_regularization', reg)

merger = hp.choice('merger', ['concat', 'sum'])

rmsp=True

params = {'lr': lr, 'epoch': epoch , 'batch_size': batch_size,'momentum': momentum , 'decay': decay, 'activation': activation, 'dataype_nhids':dataype_nhids, 'merger': merger, 'more_nhids': more_nhids, 'datatype_dropouts': datatype_dropouts, 'more_dropouts' : more_dropouts, 'rmsp': rmsp,
          'more_regularization': more_regularization, 'datatype_regularization': datatype_regularization }

nn_pipeline_params = {'params': params, 'type': 'nn_pipeline'}

#---------------------- nn_pipeline_params_custom ---------------


architecture =[[ 'cnv_AE', 'drugs'], [ 'mut_AE', 'drugs'], ['drugs', 'tissue_type']]
# architecture =[[ 'cnv_AE', 'drugs', 'tissue_type'], [ 'mut_AE', 'drugs','tissue_type']]
pre_scale = {'type': 'scale', 'params': {'feature_range': (-1, 1)}}
preprocessors = [ pre_scale ]

#sgd only params
lr =  0.001
batch_size= 10
momentum=.9
decay=1e-6
#

merger = hp.choice('merger', ['concat', 'sum'])
reg_type = hp.choice('reg_type', ['l1', 'l2'])
rmsp=True

reg_values = [0.01, 0.001, 0.0001]
drop_values = [0.1, 0.2, 0.3, 0.4, 0.5, 0.7]
layers_values = [100, 200, 300]


reg1 = hp.choice('reg1', reg_values)
reg2 = hp.choice('reg2', reg_values)
reg3 = hp.choice('reg3', reg_values)

reg11 = [reg1]
reg22 = [reg1, reg2]
reg33 = [reg1, reg2, reg3]

datatype_drop1 = hp.choice('datatype_drop1', drop_values)
datatype_drop2 = hp.choice('datatype_drop2', drop_values)
datatype_drop3 = hp.choice('datatype_drop3', drop_values)



datatype_drop11= [datatype_drop1]
datatype_drop22= [datatype_drop1, datatype_drop2]
datatype_drop33= [datatype_drop1, datatype_drop2, datatype_drop3]


more_drop1 = hp.choice('more_drop1', drop_values)
more_drop2 = hp.choice('more_drop2', drop_values)
more_drop3 = hp.choice('more_drop3', drop_values)

more_drop11= [datatype_drop1]
more_drop22= [datatype_drop1, datatype_drop2]
more_drop33= [datatype_drop1, datatype_drop2, datatype_drop3]


layer1 = hp.choice('layer1', layers_values)
layer2 = hp.choice('layer2', layers_values)
layer3 = hp.choice('layer3', layers_values)

layer11= [layer1]
layer22= [layer1, layer2]
layer33= [layer1, layer2, layer3]

dlayer1 = hp.choice('dlayer1', layers_values)
dlayer2 = hp.choice('dlayer2', layers_values)
dlayer3 = hp.choice('dlayer3', layers_values)

dlayer11= [dlayer1]
dlayer22= [dlayer1, dlayer2]
dlayer33= [dlayer1, dlayer2, dlayer3]

epoch= hp.choice('epoch', [100, 200, 300])
activation =  hp.choice('activation', ['tanh', 'sigmoid'])

data_layers = hp.choice('data_layers',
                        [ {'dataype_nhids': dlayer11, 'datatype_regularization': reg11,   'datatype_dropouts': datatype_drop11},
                          {'dataype_nhids': dlayer22, 'datatype_regularization': reg22,   'datatype_dropouts': datatype_drop22},
                          {'dataype_nhids': dlayer33, 'datatype_regularization': reg33,   'datatype_dropouts': datatype_drop33} ])

more_layers=hp.choice('more_layers',
              [
                  {'more_nhids': layer11 , 'more_dropouts': more_drop11,'more_regularization': reg11 } ,
                  { 'more_nhids': layer22 , 'more_dropouts': more_drop22,'more_regularization': reg22 } ,
                  {'more_nhids': layer33 , 'more_dropouts': more_drop33,'more_regularization': reg33 }

              ] )

params_nonlayer = {'lr': lr, 'epoch': epoch , 'batch_size': batch_size,'momentum': momentum , 'decay': decay, 'activation': activation,  'merger': merger,  'rmsp': rmsp,
           'reg_type': reg_type, 'architecture' : architecture,
          'preprocessors': preprocessors }

params = hp.choice('all',[[data_layers, more_layers, params_nonlayer ]])

nn_pipeline_params_custom = {'params': params, 'type': 'nn_pipeline_custom'}

#---------------------- choose models ---------------

# m=[random_forest]
# m=[ridge, elastic, lasso, random_forest, kernel]
# m=[ridge, elastic, lasso, random_forest, RandomForestRegressor, kernel]

# m=[elastic, ridge, lasso]
# m=[kernel]
#m = [nn_pipeline_params]


# m  = [AdaBoostDecisionTree, ridge, elastic, lasso, random_forest]
# choices = hp.choice('model', m)


# scope.define(SVR)
# space =  hp.choice('model', [
#     scope.SVR(C=C, kernel='linear'),
#      scope.SVR(C=C, kernel='rbf', width=hp.lognormal('svm_rbf_width', 0, 1))
# ])

# -----------  - multimodel  --------------------
base_models_AE = {
    'params': {'max_features': 'auto', 'splitter': 'best',
               'max_depth': hp.choice('max_depth_base_models_AE', range(30,100,5)),
               'criterion': 'friedman_mse'},
    'type': 'randomforest',
    'bag': {'n_estimators': hp.choice('n_estimators_base_models_AE', range(30,70,10)),
            'n_jobs': 1}
}

base_models_org = {
    'params': {'max_features': 'auto', 'splitter': 'best',
               'max_depth': hp.choice('max_depth_base_models_org', range(30,100,5)),
               'criterion': 'friedman_mse'},
    'type': 'randomforest',
    'bag': {'n_estimators': hp.choice('n_estimators_base_models_org', range(30,70,10)),
            'n_jobs': 1}
}

# models_sub1B = {'mu':base_models_org, 'cnv':base_models_org, 'mut_AE': base_models_AE, 'cnv_AE': base_models_AE}
models_sub1A = {'mu':base_models_org, 'mut_AE': base_models_AE, 'cnv':base_models_org, 'cnv_AE': base_models_AE,
                'ge': base_models_org, 'ge_AE': base_models_AE, 'meth':base_models_org, 'meth_AE': base_models_AE,}
model_params_multi = {'type': 'multimodels', 'params': {'models': models_sub1A }}

# --------------------------------------------------------------------------

m  = [model_params_multi]
choices = hp.choice('model', m)
