
from hyperopt import hp


d_big = [
    {
        'drug_features': {
                            'targets': True, #hp.pchoice('targets', [(0.8, True), (0.2, False)]),
                            'drug_features': True, #hp.pchoice('drug_features', [(0.8, True), (0.2, False)]),
                            'combination_factor': False, #hp.pchoice('combination_factor', [(0.8, True), (0.2, False)]),
                            'processed_targets': True, #hp.pchoice('processed_targets', [(0.8, True), (0.2, False)]),
                            'drug_info': hp.pchoice('targets', [(0.8, True), (0.2, False)]),
                            'log_scores': False, #hp.pchoice('log_scores', [(0.8, True), (0.2, False)]),
                            'challenge':1
                        },
        'load_meyth': True, #hp.choice('load_meyth', [True, False]),
        'meth_auto': hp.choice('meth_auto', [True, False]),
        # 'meyth_aggr': hp.choice('aggr', ['beta_avg', 'beta_metCount',
        #                                  'beta_sum', 'm_avg', 'm_sum']),
        'meyth_aggr': 'beta_corr_tissue', #hp.choice('aggr', ['beta_avg', 'beta_sum', 'beta_corr_tissue', 'beta_corr_cancer']),
        'load_ge':  hp.choice('load_ge', [True, False]),   # all 4 data=False is not possible
        'ge_auto': hp.choice('ge_auto',[True,False]),
        'ge_impute': True,
        'load_cnv': hp.choice('cnv', [True, False]),
        'cnv_auto': hp.choice('cnv_auto',[True,False]),
        'load_mut': hp.choice('mut', [True, False]),
        'mut_filter': None,
        'cnv_filter': None,
        'mut_auto': hp.choice('mut_auto',[True,False]),
        'auto_path': 'auto/no_filter/newCNV37/',
        'mut_agg_type': hp.choice('mut_agg_type', [0,1]),
                                    # [(0.3, 0),
                                    #  (0.5, 1),
                                    #  (0.1, 2),
                                    #  (0.1, 3),
                                    #  (0.1, 4)
                                    #  ]),
                                        # 'raw': all possible features, numeric
                                        # 0: no aggregation, relevant features
                                        # 1: 1 feature per gene ---> Best accuracy
                                        # 2: mutation_aggregation_cancer,
                                        # 3: mutation_aggregation_cancer_scaled
                                        # 4: mutation_aggregation_cancer_polynomial


        'cnv_agg_type': hp.choice('cnv_agg_type',['raw','old',0]),
        'prior_integration': 'filtering', # None #hp.choice('prior_integration', ['scaling','filtering']),
        'scaling_factor': 1, #hp.choice('scaling_factor', [200 , 500, 1000, 2000]),

        'pathways': [
                                'pathways_cancer',
                                'minoacyl-tRNA',
                                'NF-kappaB',
                                'cell_cycle',
                                # 'transforming_growth_factor-beta',
                                'apoptosis',
                                'p53_signaling',
                                'MAPK_signaling',
                                'PI3K-Akt_signaling',
                                # 'Wnt_signaling',
                                'mTOR_signaling'
                              ]
    }
]

d_small =  [
           {
                'drug_features': {
                            'targets': True,
                            'drug_features': True,
                            'combination_factor': False,
                            'processed_targets': True, #hp.choice('processed_targets', [True, False]),
                            'drug_info': hp.choice('drug_info', [True, False]),
                            'log_scores': False,
                            'challenge':1
                        },

                'load_meyth': True, 'meth_auto' : True,
                'meyth_aggr': 'beta_corr_tissue',
                'load_ge':  True, 'ge_auto':True,
                'ge_impute': True,
                'load_cnv': True, 'cnv_auto':True,
                'load_mut': True, 'mut_auto':True,
                'auto_path': 'auto/no_filter/newCNV37/',
                'mut_agg_type': 1,
                'cnv_agg_type': 0,
                'prior_integration': 'filtering',
                'scaling_factor': 100,
                'mut_filter': None,
                              # 'filters/filtering_mutation_genes_v1.csv',  #------> Best
                               #'filters/filtering_mutation_genes_v2.csv',
                'cnv_filter': None,
                              #'filters/gex_cnv_Linear_0.1.csv',
                'pathways':  ['pathways_cancer',
                                'minoacyl-tRNA',
                                'NF-kappaB',
                                'transforming_growth_factor-beta',
                                'apoptosis',
                                'p53_signaling',
                                'MAPK_signaling',
                                'PI3K-Akt_signaling',
                                'Wnt_signaling',
                                'mTOR_signaling']
           }
    ]

d_sub1b_NN =  [
           {
                'drug_features': {
                            'targets': True,
                            'drug_features': True,
                            'combination_factor': False,
                            'processed_targets': True, #hp.choice('processed_targets', [True, False]),
                            'drug_info': False,# hp.choice('drug_info', [True, False]),
                            'log_scores': False,
                            'challenge':1
                        },

                'load_meyth': False, 'meth_auto' : False,
                'meyth_aggr': 'beta_corr_tissue',
                'load_ge':  False, 'ge_auto':False,
                'ge_impute': True,
                'load_cnv': False, 'cnv_auto':True,
                'load_mut': False, 'mut_auto':True,
                'auto_path': 'auto/no_filter/newCNV37/',
                'mut_agg_type': 1,
                'cnv_agg_type': 0,
                'prior_integration': 'filtering',
                'scaling_factor': 100,
                'mut_filter': None,
                              # 'filters/filtering_mutation_genes_v1.csv',  #------> Best
                               #'filters/filtering_mutation_genes_v2.csv',
                'cnv_filter': None,
                             # 'filters/gex_cnv_Linear_0.1.csv',
                'pathways': [                       #------> Best
                                'pathways_cancer',
                                'minoacyl-tRNA',
                                'NF-kappaB',
                                'cell_cycle',
                                'transforming_growth_factor-beta',
                                'apoptosis',
                                'p53_signaling',
                                'MAPK_signaling',
                                'PI3K-Akt_signaling',
                                'Wnt_signaling',
                                'mTOR_signaling'
                              ],
           }
    ]


d_sub1b_DT =  [
           {
                'drug_features': {
                            'targets': True,
                            'drug_features': True,
                            'combination_factor': False,
                            'processed_targets': True, #hp.choice('processed_targets', [True, False]),
                            'drug_info': False,# hp.choice('drug_info', [True, False]),
                            'log_scores': False,
                            'challenge':1
                        },

                'load_meyth': False, 'meth_auto' : False,
                'meyth_aggr': 'beta_corr_tissue',
                'load_ge':  False, 'ge_auto':False,
                'ge_impute': True,
                'load_cnv': True, 'cnv_auto':True,
                'load_mut': True, 'mut_auto':True,
                'auto_path': 'auto/no_filter/newCNV37/',
                'mut_agg_type': 1,
                'cnv_agg_type': 0,
                'prior_integration': 'filtering',
                'scaling_factor': 100,
                'mut_filter': None,
                              # 'filters/filtering_mutation_genes_v1.csv',  #------> Best
                               #'filters/filtering_mutation_genes_v2.csv',
                'cnv_filter': None,
                             # 'filters/gex_cnv_Linear_0.1.csv',
                'pathways': [                       #------> Best
                                'pathways_cancer',
                                'minoacyl-tRNA',
                                'NF-kappaB',
                                'cell_cycle',
                                'transforming_growth_factor-beta',
                                'apoptosis',
                                'p53_signaling',
                                'MAPK_signaling',
                                'PI3K-Akt_signaling',
                                'Wnt_signaling',
                                'mTOR_signaling'
                              ],
           }
    ]

d_sub1a_DT =  [
           {
                'drug_features': {
                            'targets': True,
                            'drug_features': True,
                            'combination_factor': False,
                            'processed_targets': True, #hp.choice('processed_targets', [True, False]),
                            'drug_info': False,# hp.choice('drug_info', [True, False]),
                            'log_scores': False,
                            'challenge':1
                        },

                'load_meyth': True, 'meth_auto' : True,
                'meyth_aggr': 'beta_corr_tissue',
                'load_ge':  True, 'ge_auto':True,
                'ge_impute': True,
                'load_cnv': True, 'cnv_auto':True,
                'load_mut': True, 'mut_auto':True,
                'auto_path': 'auto/no_filter/newCNV37/',
                'mut_agg_type': 1,
                'cnv_agg_type': 0,
                'prior_integration': 'filtering',
                'scaling_factor': 100,
                'mut_filter': None,
                              # 'filters/filtering_mutation_genes_v1.csv',  #------> Best
                               #'filters/filtering_mutation_genes_v2.csv',
                'cnv_filter': None,
                             # 'filters/gex_cnv_Linear_0.1.csv',
                'pathways': [                       #------> Best
                                'pathways_cancer',
                                'minoacyl-tRNA',
                                'NF-kappaB',
                                'cell_cycle',
                                'transforming_growth_factor-beta',
                                'apoptosis',
                                'p53_signaling',
                                'MAPK_signaling',
                                'PI3K-Akt_signaling',
                                'Wnt_signaling',
                                'mTOR_signaling'
                              ],
           }
    ]

#choices = hp.choice('data',d_big)
choices = hp.choice('data', d_sub1a_DT)


'''
 'pathways': hp.pchoice('pathways',
                                      [
                                          (0.05, ['pathways_cancer', 'misreulation_cancer']),
                                          (0.05, ['misreulation_cancer']),
                                          (0.05, ['pathways_cancer']),
                                          (0.05,  ['pathways_cancer','PI3K-Akt_signaling']),
                                          # (0.05, ['pathways_cancer','PI3K-Akt_signaling', 'cell_cycle']),
                                          (0.3, [ 'pathways_cancer',
                                                'minoacyl-tRNA',
                                                'NF-kappaB',
                                                'transforming_growth_factor-beta',
                                                'apoptosis',
                                                'p53_signaling',
                                                'MAPK_signaling',
                                                'PI3K-Akt_signaling',
                                                'Wnt_signaling',
                                                'mTOR_signaling'
                                      ]),
                                          (0.5, ['pathways_cancer',
                                                # 'minoacyl-tRNA',
                                                'NF-kappaB',
                                                # 'transforming_growth_factor-beta',
                                                # 'apoptosis',
                                                'p53_signaling',
                                                'MAPK_signaling',
                                                'PI3K-Akt_signaling',
                                                # 'Wnt_signaling',
                                                'mTOR_signaling'
                                      ])
                ]) '''
