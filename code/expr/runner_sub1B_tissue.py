__author__ = 'haitham'

from dataLoader.data_low_level_clean import phase
from dataLoader.combin_priority import generate_combin_priority
from train_predict import run
import numpy as np
import datetime
import logging
import sys
import os

model_params = {'params': {'verbose': 2,
                           'activation': 'sigmoid',
                           'saveModels': None,
                           'batch_size': 10,
                           'more_regularization': (0.001, 0.1),
                           'more_nhids': (200, 100),
                           'early_stopping': None,
                           'datatype_regularization': (0.001,),
                           'decay': 1e-06, 'merger': 'concat',
                           'datatype_dropouts': (0.2,),
                           'dataype_nhids': (100,),
                           'epoch': 400,
                           'rmsp': True,
                           'lr': 0.001,
                            'architecture': ( ('cnv_AE', 'drugs', 'tissue_type'),  ('mut_AE', 'drugs', 'tissue_type'), ('mut_PCA', 'cnv_PCA','drugs', 'tissue_type'), ('drugs', 'tissue_type')),
                           'more_dropouts': (0.4, 0.4),
                           'account_for_drugs': True,
                           'momentum': 0.9,
                           'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)},
                'type': 'nn_pipeline_custom'}

# pre_params ={'type': 'standard'}
pre_params = {'type': 'smart', 'params': {'type': 'standard'}}

data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0,
                   'auto_path': 'auto/no_filter/test1', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None,
                   'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'),
                   'cnv_pca': True, 'prior_integration': 'filtering',
                   'ge_pca': False, 'scaling_factor': 100,
                   'load_cnv': False, 'mut_agg_type': 1,
                   'add_tissues': True, 'cnv_auto': True,
                   'meth_auto': False,
                   'pca_path': 'pca/no_filter',
                   'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False,
                   'mut_pca': True, 'meth_pca': False, 'load_ge': False,
                   'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}



# --------------------  multimodel Random Forest -------------------------------/

# base_models_AE = {
#     'params': {'max_features': 'auto', 'splitter': 'best', 'max_depth': 70, 'criterion': 'friedman_mse'},
#     'type': 'randomforest',
#     'bag': {'n_estimators': 50, 'n_jobs': 3}
# }
#
# base_models_org = {
#     'params': {'max_features': 'auto', 'splitter': 'best', 'max_depth': 70, 'criterion': 'friedman_mse'},
#     'type': 'randomforest',
#     'bag': {'n_estimators': 50, 'n_jobs': 3}
# }
#
# models_sub1B = {'mu':base_models_org, 'cnv':base_models_org, 'mut_AE': base_models_AE, 'cnv_AE': base_models_AE}
# model_params_multi = {'type': 'multimodels', 'params': {'models': models_sub1B }}

# ---------------------------------------------------------------------------



# setup logging
base_folder ='../logs'
timeStamp = '_{0:%b}-{0:%d}_{0:%H}-{0:%M}'.format(datetime.datetime.now())
filename= os.path.basename(__file__)
filename, file_extension = os.path.splitext(filename)
filename = os.path.join(base_folder,filename+ timeStamp+'.log')
print filename
logging.basicConfig(filename = filename,
                    filemode='w',
                    format='%(asctime)s - {%(filename)s:%(lineno)d} - %(message)s',
                    datefmt='%m/%d %I:%M',
                    level=logging.INFO) # or logging.DEBUG
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

details = 'sub1B final submission'
logging.info(details)
# random_seed= 3003495
# random_seed= 713990
random_seed= 7703283
np.random.seed(random_seed)
logging.info('random seed')
logging.info(random_seed)

args = {}
args['data_params'] = data_params
args['model_params'] = model_params
args['pre_params'] = pre_params
logging.info("Args: %s", args)

#-------------------   generate submission  ----------------#

save_path = '../../results/sub1b/tissue400/'
logging.info('Generating submission file in %s', save_path)

#---> Score leaderboard
global_scores, final_score, tiebreaker_score = run(args, data_phase=phase.leaderboard, save_path= save_path)

#---> Generate submission for test data
run(args, data_phase=phase.test, save_path= save_path)

generate_combin_priority()
logging.info("VTForce2")
logging.info('Submission file saved in %s', save_path)
logging.info("Leaderboard scores: ")
logging.info("Primary Metric: %s", -final_score)
logging.info("Tie Breaking: %s", -tiebreaker_score)
logging.info('Tissue_type used=%s', data_params['add_tissues'])