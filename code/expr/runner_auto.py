from functools import partial

__author__ = 'marakeby'
import hyperopt
from hyperopt import fmin, tpe, Trials
from params import params_data, params_model, params_preprocess
from train_predict import run_cv_single_score

from matplotlib import pyplot as plt
import numpy as np
import logging, sys, json, datetime
import yaml

# setup logging
logging.basicConfig(filename='../logs/runner_auto.log',
                    filemode='w',
                    format='%(asctime)s - {%(filename)s:%(lineno)d} - %(message)s',
                    datefmt='%m/%d %I:%M',
                    level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
logging.info("Start runner_auto")


#choose which data file to use here
space  =  {'data_params': params_data.choices,
            'model_params': params_model.choices,
           'pre_params': params_preprocess.choices}

# random seed for reproducibility
nn_seed = 911787
np.random.seed(nn_seed)
logging.info('random state')
logging.info(nn_seed)
trials = Trials()

run_cv_single_score_part = partial(run_cv_single_score, data_random_state=nn_seed)
best = fmin(run_cv_single_score_part, space, algo=tpe.suggest, max_evals=300, trials=trials)


logging.info("~~~~~~~~~~~~~~~~~ fmin Done ~~~~~~~~~~~~~~~~~")
logging.info("Best Model:")
logging.info(hyperopt.space_eval(space, best))
min = np.min(trials.results)
logging.info("Best loss is: %s", min['loss'])

# Save output
timeStamp = '_{0:%b}-{0:%d}_{0:%H}-{0:%M}'.format(datetime.datetime.now())

tests = list()
logging.info("Objective Func values for all trials: ")
for trial in trials.trials:
    rval = {}
    for k, v in trial['misc']['vals'].items():
        if v:
            rval[k] = v[0]

    test = hyperopt.space_eval(space, rval)
    test['result'] = trial['result']['loss']
    tests.append(test)
    logging.info(test['result'])


#~~~~ Writing yaml data
with open('../results/batch_trials'+timeStamp+'.yaml', 'w') as f:
     yaml.dump(tests, f)

# Reading data back
# with open('../results/trials2fixed'+timeStamp+'.yaml', 'r') as f:
#      tests = yaml.load(f)

# print(tests)
#~~~ Run one of the tests again
# run(tests[0])


#~~~~ Plots
# ids = [t['tid'] for t in trials.trials]
# cases = [t['misc']['vals']['data']['scaling_factor'] for t in trials.trials] # aggr is the name of the choice
# plt.xlabel("Trial index")
# plt.ylabel('Parameter aggr (methyl aggregation method)')
# plt.title("Values used during random search")
# plt.scatter(ids, cases)
#
# fvals = [t['result']['loss'] for t in trials.trials]
# scaling_factor = [t['misc']['vals']['data']['scaling_factor'] for t in trials.trials]
# plt.xlabel("Trial index")
# plt.ylabel('Parameter: scaling_factor')
# plt.title("Effect of scaling_factor druring random search")
# plt.scatter(scaling_factor, fvals)