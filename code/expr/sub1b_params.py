
pre_params = {'type': 'standard'}

data_params =  {
                'subchallenge': 'B',
                'add_tissues': True,       # if False, no tissue info is used
                'drug_features': {
                            'subchallenge': 'B',
                            'targets': True,
                            'drug_features': True,
                            'combination_factor':False,
                            'processed_targets': True,
                            'drug_info': False,
                            'log_scores': False,
                            'challenge':1,
                            'drug_auto' : None
                        },
                'pca_path' : 'pca/no_filter',
                # 'pca_path' : 'pca/full_drug_pathways'

                # 'auto_path': 'auto/full_drug_pathways/',
                'auto_path': 'auto/no_filter',



                'load_meyth': False, 'meth_auto' : False, 'meth_pca' : False,
                'meyth_aggr':  'beta_corr_tissue', # corr with ge, by tissue type #------> best
                                # 'beta_sum',     # no imputation, no corr, sum per gene
                'load_ge':  False, 'ge_auto':False, 'ge_pca' :False,
                'ge_impute': True,
                'load_cnv': False, 'cnv_auto':True, 'cnv_pca':True,
                'load_mut': False, 'mut_auto':True, 'mut_pca' : True,
                'mut_filter': None,
                'cnv_filter': None,

                'mut_agg_type': 1,  # 'raw': all possible features, numeric
                                    # 0: no aggregation, relevant features
                                    # 1: 1 feature per gene                # -----> Best
                'cnv_agg_type': 0,
                                    # 'raw: zygosity as categorical (6 features)
                                    # 0: zygosity as numeric (4 features)    #------> Best
                                    # 1: aggregation into one number
                'prior_integration': 'filtering',
                                     # None,  # no filtering of molecular data

                'pathways': [                       #------> Best
                                'pathways_cancer',
                                'minoacyl-tRNA',
                                'NF-kappaB',
                                'cell_cycle',
                                'transforming_growth_factor-beta',
                                'apoptosis',
                                'p53_signaling',
                                'MAPK_signaling',
                                'PI3K-Akt_signaling',
                                'Wnt_signaling',
                                'mTOR_signaling'
                              ]
               }

def get_model3():
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.001), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,), 'epoch': 300, 'rmsp': True, 'lr': 0.001, 'architecture': (('cnv', 'cnv_AE', 'drugs'), ('mu', 'mut_AE', 'drugs'), ('drugs', 'tissue_type')), 'more_dropouts': (0.2, 0.2), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}
    pre_params = {'type': 'standard'}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter/test1', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'mTOR_signaling'), 'cnv_pca': False, 'prior_integration': 'filtering', 'ge_pca': False, 'scaling_factor': 100, 'load_cnv': True, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': False, 'pca_path': 'pca/no_filter', 'load_mut': True, 'tranform_by_DT': False, 'ge_auto': False, 'mut_pca': False, 'meth_pca': False, 'load_ge': False, 'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}
    return model_params, pre_params, data_params


def get_model8():
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.001), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001, 0.001), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.4, 0.2), 'dataype_nhids': (200, 100), 'epoch': 300, 'rmsp': True, 'lr': 0.001, 'architecture': (('cnv_AE', 'drugs', 'tissue_type'), ('mut_AE', 'drugs', 'tissue_type'), ('drugs', 'tissue_type')), 'more_dropouts': (0.4, 0.4), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}
    pre_params = {'type': 'standard'}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter/test2', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'), 'cnv_pca': False, 'prior_integration': 'filtering', 'ge_pca': False, 'scaling_factor': 100, 'load_cnv': False, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': False, 'pca_path': 'pca/no_filter', 'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False, 'mut_pca': False, 'meth_pca': False, 'load_ge': False, 'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}
    return model_params, pre_params, data_params


def get_model10():
    model_params = {'type': 'nn_pipeline_custom', 'params': {'datatype_regularization': (0.0001,),
                                                             'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,),
                                                             'epoch': 200, 'batch_size': 10, 'more_regularization': (0.0001,),
                                                             'rmsp': True, 'lr': 0.001, 'preprocessors': [{'params': {'feature_range': (-1, 1)},
                                                                                                           'type': 'scale'}],
                                                             'more_nhids': (300,),
                                                             'more_dropouts': (0.2,),
                                                             'architecture': (('cnv_AE', 'drugs'), ('mut_AE', 'drugs'), ('drugs', 'tissue_type')),
                                                             'activation': 'sigmoid', 'momentum': 0.9, 'reg_type': 'l2', 'verbose':2}}
    pre_params = {'type': None}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter/test1', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'), 'cnv_pca': False, 'prior_integration': 'filtering', 'ge_pca': False, 'scaling_factor': 100, 'load_cnv': False, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': False, 'pca_path': 'pca/no_filter', 'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False, 'mut_pca': False, 'meth_pca': False, 'load_ge': False, 'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}

    return model_params, pre_params, data_params


def get_model12():
    model_params = {'type': 'nn_pipeline_custom', 'params': {'datatype_regularization': (0.0001,),
                                                             'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,),
                                                             'epoch': 200, 'batch_size': 10, 'more_regularization': (0.0001,),
                                                             'rmsp': True, 'lr': 0.001, 'preprocessors': [{'params': {'feature_range': (-1, 1)},
                                                                                                           'type': 'scale'}],
                                                             'more_nhids': (300,),
                                                             'more_dropouts': (0.2,),
                                                             'architecture': (('cnv_AE','mut_AE', 'drugs'), ('cnv_AE', 'mut_AE', 'drugs'), ('drugs', 'tissue_type')),
                                                             'activation': 'sigmoid', 'momentum': 0.9, 'reg_type': 'l2', 'verbose':2}}
    pre_params = {'type': None}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter/test1', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'), 'cnv_pca': False, 'prior_integration': 'filtering', 'ge_pca': False, 'scaling_factor': 100, 'load_cnv': False, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': False, 'pca_path': 'pca/no_filter', 'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False, 'mut_pca': False, 'meth_pca': False, 'load_ge': False, 'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}

    return model_params, pre_params, data_params


def get_model16():
    model_params = {'params': {'datatype_regularization': (0.001, 0.001, 0.001),
                               'verbose': 2, 'decay': 1e-06,
                               'merger': 'concat', 'activation': 'sigmoid',
                               'saveModels': None, 'dataype_nhids': (100,),
                               'epoch': 300, 'batch_size': 10, 'more_regularization': (0.01, 0.01, 0.01),
                               'rmsp': True, 'more_dropouts': (0.4, 0.4, 0.4), 'lr': 0.001,
                               'architecture': (('cnv_AE', 'drugs'), ('mut_AE', 'drugs'), ('drugs', 'tissue_type')),
                               'more_nhids': (200, 100), 'early_stopping': None, 'account_for_drugs': True,

                               'datatype_dropouts': (0.3, 0.3, 0.3), 'momentum': 0.9,
                               'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)},
                    'type': 'nn_pipeline_custom'}

    pre_params = {'type': 'standard'}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter/test1', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'), 'cnv_pca': False, 'prior_integration': 'filtering', 'ge_pca': False, 'scaling_factor': 100, 'load_cnv': False, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': False, 'pca_path': 'pca/no_filter', 'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False, 'mut_pca': False, 'meth_pca': False, 'load_ge': False, 'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}

    return model_params, pre_params, data_params


def get_model1a_b():
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.1), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,), 'epoch': 300, 'rmsp': True, 'lr': 0.001, 'architecture': ( ('cnv_AE', 'drugs'),  ('mut_AE', 'drugs'), ('drugs', 'tissue_type')), 'more_dropouts': (0.4, 0.4), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}

    pre_params ={'type': 'standard'}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter/test1', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'), 'cnv_pca': False, 'prior_integration': 'filtering', 'ge_pca': False, 'scaling_factor': 100, 'load_cnv': False, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': False, 'pca_path': 'pca/no_filter', 'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False, 'mut_pca': False, 'meth_pca': False, 'load_ge': False, 'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}

    return model_params, pre_params, data_params


def get_model1a_b_pca():
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.1), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,), 'epoch': 300, 'rmsp': True, 'lr': 0.001,
                               'architecture': ( ('cnv_AE', 'drugs'),  ('mut_AE', 'drugs'), ('mut_PCA', 'cnv_PCA','drugs'), ('drugs', 'tissue_type')), 'more_dropouts': (0.4, 0.4), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}

    pre_params ={'type': 'standard'}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0,
                   'auto_path': 'auto/no_filter', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None,
                   'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'),
                   'cnv_pca': True, 'prior_integration': 'filtering',
                   'ge_pca': False, 'scaling_factor': 100,
                   'load_cnv': False, 'mut_agg_type': 1,
                   'add_tissues': True, 'cnv_auto': True,
                   'meth_auto': False,
                   'pca_path': 'pca/no_filter',
                   'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False,
                   'mut_pca': True, 'meth_pca': False, 'load_ge': False,
                   'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}

    return model_params, pre_params, data_params


def get_model1a_b_pca_smart(): #best
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.1), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,), 'epoch': 300, 'rmsp': True, 'lr': 0.001,
                               'architecture': ( ('cnv_AE', 'drugs'),  ('mut_AE', 'drugs'), ('mut_PCA', 'cnv_PCA','drugs'), ('drugs', 'tissue_type')), 'more_dropouts': (0.4, 0.4), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}

    # pre_params ={'type': 'standard'}
    pre_params = {'type': 'smart', 'params': {'type': 'standard'}}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0,
                   'auto_path': 'auto/no_filter', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None,
                   'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'),
                   'cnv_pca': True, 'prior_integration': 'filtering',
                   'ge_pca': False, 'scaling_factor': 100,
                   'load_cnv': False, 'mut_agg_type': 1,
                   'add_tissues': True, 'cnv_auto': True,
                   'meth_auto': False,
                   'pca_path': 'pca/no_filter',
                   'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False,
                   'mut_pca': True, 'meth_pca': False, 'load_ge': False,
                   'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}

    return model_params, pre_params, data_params


def get_model1a_b_pca_smart_tissue():
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.1), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,),
                               'epoch': 400, 'rmsp': True, 'lr': 0.001,
                               'architecture': ( ('cnv_AE', 'drugs', 'tissue_type'),  ('mut_AE', 'drugs', 'tissue_type'), ('mut_PCA', 'cnv_PCA','drugs', 'tissue_type'), ('drugs', 'tissue_type')), 'more_dropouts': (0.4, 0.4), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}

    # pre_params ={'type': 'standard'}
    pre_params = {'type': 'smart', 'params': {'type': 'standard'}}
    data_params = {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0,
                   'auto_path': 'auto/no_filter', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None,
                   'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'),
                   'cnv_pca': True, 'prior_integration': 'filtering',
                   'ge_pca': False, 'scaling_factor': 100,
                   'load_cnv': False, 'mut_agg_type': 1,
                   'add_tissues': True, 'cnv_auto': True,
                   'meth_auto': False,
                   'pca_path': 'pca/no_filter',
                   'load_mut': False, 'tranform_by_DT': False, 'ge_auto': False,
                   'mut_pca': True, 'meth_pca': False, 'load_ge': False,
                   'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'B'}

    return model_params, pre_params, data_params


def get_model1a_b_pca_smart_tissue_400():   #-----> best
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.1), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,),
                               'epoch': 400, 'rmsp': True, 'lr': 0.001,
                               'architecture': ( ('cnv_AE', 'drugs', 'tissue_type'),  ('mut_AE', 'drugs', 'tissue_type'), ('mut_PCA', 'cnv_PCA','drugs', 'tissue_type'), ('drugs', 'tissue_type')), 'more_dropouts': (0.4, 0.4), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}

    pre_params = {'type': 'smart', 'params': {'type': 'standard'}}
    data_params = {
                    'subchallenge': 'B', 'add_tissues': True,
                    'auto_path': 'auto/no_filter', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None,
                    'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'),
                    'mut_pca': True, 'mut_auto': True, 'load_mut': False,
                    'cnv_pca': True, 'cnv_auto': True, 'load_cnv': False,
                    'ge_pca': False, 'load_ge': False, 'ge_auto': False,  'ge_impute': False,
                    'meth_auto': False, 'meth_pca': False, 'load_meyth': False,
                    'mut_agg_type': 1,
                    'cnv_agg_type': 0,
                    'pca_path': 'pca/no_filter',
                    'prior_integration': 'filtering',
                    'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'B', 'processed_targets': True, 'drug_features': True, 'targets': True}}

    return model_params, pre_params, data_params


# ----------------- Choose model --------------------------

model_params, pre_params, data_params = get_model1a_b_pca_smart_tissue_400()