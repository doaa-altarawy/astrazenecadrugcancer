import multiprocessing
from os import listdir
from sklearn.cross_validation import train_test_split, StratifiedKFold
from matplotlib import pyplot as plt
from models.models_factory import get_model
from dataLoader.data_factory_clean import *
from dataLoader.data_low_level_clean import phase,load_tissues
import logging
from preprocessing import pre
from scoring.scoring_R_clean import *
from dataLoader.generate_processed_data import download_synapse_data


def create_multiindex(drug, molecular_data):
    repeats = len(molecular_data.columns.levels)-1 # assume multiindex
    l = [drug.columns.values] * repeats
    n = drug.shape[1]
    columns = [['drugs']*n]
    columns.extend(l)
    # print len(columns)
    drug.columns = pd.MultiIndex.from_arrays(columns)
    drug.index.name = 'CELL_LINE'
    return drug


def shuffle(df, n=1, axis=0):
    df = df.copy(deep=True)
    for _ in range(n):
        df.apply(np.random.shuffle, axis=axis)
    return df


def prepare_inputs(params, data_phase = phase.training, multiindex=False):
    logging.info("Preparing Data with params: "
                 "\n~~~~~~~~~~~~~~~~~~~~~~~~\n %s", params)

    if not os.path.exists('../../data'):
        logging.info('Downloading missing synapse data..')
        download_synapse_data()

    drug = get_drug(params['drug_features'], data_phase = data_phase)

    data_loaded = get_data(params, multiindex=multiindex)
    if not data_loaded is None:
        if multiindex:

            drug = create_multiindex(drug,data_loaded) # return drug dataframe with multiindex. First level contain the words 'drugs' the second level is the different columns as usual
            drug.reset_index(inplace=True)
            drug.rename(columns={'index': ('CELL_LINE','','')}, inplace=True)

            data_loaded.reset_index(inplace=True)
            data_loaded.rename(columns={'index': ('CELL_LINE','','')}, inplace=True)
            merged = pd.merge(drug, data_loaded, how='left')


        else:
            drug.reset_index(inplace=True)
            data_loaded.reset_index(inplace=True)
            data_loaded.rename(columns={'index': 'CELL_LINE'}, inplace=True)
            merged = pd.merge(drug, data_loaded, on='CELL_LINE', how='left')

        merged.fillna(0, inplace=True)
        logging.info('Final merged Drug and Data shape: %s', merged.shape)

        data = merged
    else:
        data = drug


    if multiindex:

        info = pd.DataFrame(data.loc[:,   [( 'CELL_LINE','',''), ('drugs', 'COMBINATION_ID','COMBINATION_ID')]])
        info.columns= ['CELL_LINE','COMBINATION_ID']
        y = np.array(data['drugs','SYNERGY_SCORE']) # use the multiindex to get the desired column
        x = data  # wrong x, remove SYNERGY_SCORE
        x.__delitem__(('drugs', 'SYNERGY_SCORE'))
        x = x.convert_objects(convert_numeric=True)
        x = x._get_numeric_data()

        logging.debug("Data + Drug: %s", x.head())
    else:
        info = data.loc[:, [ 'CELL_LINE', 'COMBINATION_ID']]
        y = np.array(data.SYNERGY_SCORE)
        x = data  # wrong x, remove SYNERGY_SCORE
        x.__delitem__('SYNERGY_SCORE')

        x = x.convert_objects(convert_numeric=True)
        logging.debug("Data + Drug: %s", x.head())
        x = x._get_numeric_data()

        x= x.as_matrix()

    logging.info('y shape: %s', y.shape)
    logging.info('x shape: %s', x.shape)

    return x, y, info

def get_df(info, col_name, value):
    info = pd.DataFrame(info)
    df = info.copy()
    df[col_name] = value
    return df

def visualize(data, scores, args, save_vis =False):
    data_params = args['data_params']
    model_params = args['model_params']


    score_corr_train , score_corr_test = scores

    y_train, y_test, pred_train_value, pred_test_value = data


    traning_parameters = 'running information: ' + model_params['type'] + \
                             ' ,methylation: ' + str(data_params['load_meyth']) + \
                             ', gex: ' + str(data_params['load_ge']) + \
                             ', cnv: ' + str(data_params['load_cnv']) + \
                             ', mutation: ' + str(data_params['load_mut']) + \
                             ', prior_integration: ' + str(data_params['prior_integration'])

    plt.suptitle(traning_parameters + ',pathways: ' + str(data_params['pathways']), fontsize=11)


    # plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(y_test, 'b')
    plt.plot(pred_test_value, 'r')
    plt.legend(['truth', 'prediction'])
    plt.title('Testing performance' + '  testing correlation score: ' + str(score_corr_test), fontsize=8)
    # plt.show()

    # plt.figure()
    plt.subplot(2, 1, 2)
    plt.plot(y_train, 'b')
    plt.plot(pred_train_value, 'r')
    plt.legend(['truth', 'prediction'])
    plt.title('Training performance' + '  training correlation score: ' + str(score_corr_train), fontsize=8)

    if save_vis:
        plt.savefig('../results/fig/' + traning_parameters + '.png')

    plt.show()
    plt.close()

def score_saved_model(params):
    X_train = params['X_train']
    X_test = params['X_test']
    y_train = params['y_train']
    y_test = params['y_test']
    info_train = params['info_train']
    info_test = params['info_test']
    multiindex = params['multiindex']
    args = params['args']
    full_model_name =  params['loadSavedModel']


    model_params = args['model_params']


    model = get_model(model_params)
    print 'model name', full_model_name
    model.load_weights(X_train,full_model_name)
    pred_lb1 = model.predict(X_test)
    obs_test = get_df(info_test, 'SYNERGY_SCORE', y_test)
    pred_test = get_df(info_test, 'PREDICTION', pred_lb1)
    global_score, final_score, tiebreaker_score = getAllScores_ch1(obs_test, pred_test)
    return global_score, final_score, tiebreaker_score

def run_online(iteration_id, args):

    multiindex = True
    data_params = args['data_params']
    pre_params = args['pre_params']
    model_params = args['model_params']

    models_save_path = model_params['params']['saveModels']['save_folder']

    models_save_path_tr = os.path.join(models_save_path, str(iteration_id), 'training')
    models_save_path_lb =os.path.join(models_save_path, str(iteration_id), 'validation')

    args['model_params']['params']['saveModels']['save_folder'] = models_save_path_tr
    if not os.path.exists(models_save_path_tr):
        os.makedirs(models_save_path_tr)

    if not os.path.exists(models_save_path_lb):
        os.makedirs(models_save_path_lb)


    #load Traiing dataset
    logging.info('load Training dataset')
    X_tr, y_tr, info_tr= prepare_inputs(data_params, phase.training, multiindex = multiindex)

    #load LB dataset
    logging.info('load LB dataset')
    X_lb, y_lb, info_lb= prepare_inputs(data_params, phase.leaderboard, multiindex = multiindex)
    X_lb1, X_lb2, y_lb1, y_lb2, info_lb1, info_lb2 = train_test_split(X_lb, y_lb, info_lb, test_size=0.5)

    #train on the whole training set
    inn = {'X_train': X_tr,'X_test': X_lb1,'y_train':y_tr, 'y_test': y_lb1,
               'info_train': info_tr,'info_test': info_lb1,
               'multiindex': multiindex ,'args': args,
               'loadSavedModel': None}
    run_fold(inn)



    #select models
    logging.info('select models')
    all_models= listdir(models_save_path_tr)
    import random
    ind = random.sample(range(0, len(all_models)), 10)
    print ind
    selected_models = [all_models[i] for i in ind]
    logging.info(selected_models)

    #score models on validation (LB1)
    global_scores_lb1, final_score_lb1, tiebreaker_scores_lb1 = [], [],[]
    logging.info('score models on LB1')
    for model_name in selected_models:
        full_model_name = os.path.join(models_save_path_tr, model_name)
        inn['loadSavedModel'] = full_model_name
        global_score, final_score, tiebreaker_score = score_saved_model(inn)
        global_scores_lb1.append(global_score)
        final_score_lb1.append(final_score)
        tiebreaker_scores_lb1.append(tiebreaker_score)


    # train models on LB1 and score on LB2
    args['model_params']['params']['saveModels']['save_folder'] = models_save_path_lb

    logging.info('train models on LB1 and score on LB2')
    inputs =[]
    for model_name in selected_models:
        full_model_name = os.path.join(models_save_path_tr, model_name)
        inn = {'X_train': X_lb1,'X_test': X_lb2,'y_train':y_lb1, 'y_test': y_lb2, 'info_train': info_lb1,'info_test': info_lb2,
               'multiindex': multiindex ,'args': args,
               'loadSavedModel': full_model_name}
        inputs.append(inn)

    pool =multiprocessing.Pool(10)
    results= pool.map(run_fold, inputs)

    global_scores_lb2, final_score_lb2, tiebreaker_scores_lb2 = [], [],[]
    for i in range(10):
        scores = results[i][0]
        predictions = results[i][1]
        global_score, final_score, tiebreaker_score, global_scores_train, final_scores_train, tiebreaker_score_train =scores
        global_scores_lb2.append(global_score)
        final_score_lb2.append(final_score)
        tiebreaker_scores_lb2.append(tiebreaker_score)


    logging.info("\n")
    logging.info("LB2 global scores %s",global_scores_lb2)
    logging.info("LB2 Final scores %s",final_score_lb2)
    logging.info("LB2 Tiebreaker scores %s",tiebreaker_scores_lb2)
    logging.info("\n")
    logging.info("LB1 global scores %s",global_scores_lb1)
    logging.info("LB1 Final scores %s",final_score_lb1)
    logging.info("LB1 Tiebreaker scores %s",tiebreaker_scores_lb1)
    logging.info("\n")

    plt.plot(final_score_lb2)
    plt.plot(final_score_lb1)
    plt.legend(['lb2', 'lb1'])
    plt.show()


def get_train_test_data(args, data_phase = phase.training, challenge =1):
    data_params = args['data_params']
    model_params = args['model_params']
    pre_params = args['pre_params']
    print pre_params
    print '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
    logging.info(args)

    if model_params['type'] in ['multikernel' , 'nn_pipeline' ,'multimodels' ,'nn_pipeline_custom']:
        multiindex =True
    else:
        multiindex =False

    # get data
    x, y, info = prepare_inputs(data_params, phase.training, multiindex= multiindex)

    if data_phase == phase.training:
        X_train, X_test, y_train, y_test, info_train, info_test = train_test_split(x, y, info, test_size=0.3)
    elif data_phase == phase.leaderboard:
        print('leaderboard')
        X_train = x
        y_train = y
        info_train = info
        if challenge ==2:
            data_params['drug_features']['challenge'] = 2
        X_test, y_test, info_test = prepare_inputs(data_params, phase.leaderboard, multiindex = multiindex)

    elif data_phase == phase.test:
        print('test')
        X_lb, y_lb, info_lb = prepare_inputs(data_params, phase.leaderboard, multiindex = multiindex)
        info_train =  pd.concat((info,info_lb ), axis=0)
        if multiindex:
            X_train =  pd.concat((x, X_lb), axis=0)
            y_train =  np.concatenate((y, y_lb), axis=0)
        else:
            X_train =  np.concatenate((x, X_lb), axis=0)
            y_train =  np.concatenate((y, y_lb), axis=0)

        X_test, y_test, info_test = prepare_inputs(data_params, phase.test, multiindex= multiindex)

        #remove y outliers
        y_train = pre.remove_outliers(y_train)


        proc = pre.get_processor(pre_params)

        if proc:
            if pre_params['type'] == 'tissue-specific':
                X_train, X_test, y_train, y_test = preprocess_tissue(X_train, X_test, y_train, y_test, proc, multiindex, info_train, info_test)
            else:
                X_train, X_test= preprocess(X_train, X_test, proc, multiindex)
        else:
           print('no preprocessing applied')


    return X_train, X_test, y_train, y_test, info_train, info_test, multiindex


def run(args, data_phase = phase.training, vis=False, save_vis=False, save_path='', challenge  = 1, loadSavedModel=None):
    logging.info("Running new experiment\n========================\n")

    X_train, X_test, y_train, y_test, info_train, info_test, multiindex = get_train_test_data(args, data_phase = data_phase)

    inn = {'X_train': X_train,'X_test': X_test,'y_train':y_train, 'y_test': y_test,
           'info_train': info_train,'info_test': info_test,
           'multiindex': multiindex ,'args': args,
           'loadSavedModel': loadSavedModel}
    scores, predictions = run_fold(inn)
    pred_test, pred_train = predictions
    global_scores, final_score, tiebreaker_score, global_scores_train, final_scores_train, tiebreaker_score_train =scores

    #saving---------------------
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    if data_phase ==phase.leaderboard:
        print ('pred_test shape', pred_test.shape)
        pred_test.to_csv(os.path.join(save_path, 'prediction.csv'), index=False)

    if data_phase ==phase.test:
        pred_test.to_csv(os.path.join(save_path,'prediction.csv'), index=False)

    if data_phase ==phase.training:
        pred_test.to_csv(os.path.join(save_path,'prediction.csv'), index=False)

    return -global_scores, -final_score, -tiebreaker_score

def preprocess(X_train, X_test, proc, multiindex):
    # multiindex = False

    if multiindex:
        ix = X_train.index
        c = X_train.columns

        ix_test = X_test.index
        c_test = X_test.columns

        # proc.fit(X_train.as_matrix())
        proc.fit(X_train)
        X_train = proc.transform(X_train)
        X_test = proc.transform(X_test)

        X_train = pd.DataFrame(X_train, index = ix, columns=c)
        X_test = pd.DataFrame(X_test, index = ix_test, columns=c_test)
    else:
        proc.fit(X_train)
        X_train = proc.transform(X_train)
        X_test = proc.transform(X_test)
    return X_train, X_test

def preprocess_tissue(X_train, X_test, y_train, y_test, proc, multiindex, info_train, info_test):
    # multiindex = False

    if multiindex:
        ix = X_train.index
        c = X_train.columns

        ix_test = X_test.index
        c_test = X_test.columns

        proc.fit(X_train)
        X_train = proc.transform(X_train, info_train, y_train)
        X_test = proc.transform(X_test, info_test, y_test)

        X_train = pd.DataFrame(X_train, index = ix, columns=c)
        X_test = pd.DataFrame(X_test, index = ix_test, columns=c_test)
    else:
        proc.fit(X_train, info_train, y_train)
        X_train, y_train = proc.transform(X_train, info_train, y_train)

        X_test, y_test = proc.transform(X_test, info_test, y_test)
    return X_train, X_test, y_train, y_test

# ------------------------------------------------------------------
def run_cv_single_score(args, data_random_state):
    '''
        For the hyperopt, returns a single score
    :param args:
    :return: global corr (for now)
    '''
    global_corr_test_avg, final_score_test_avg, tiebreaker_score_test_avg = run_cv(data_random_state, args, data_phase = phase.leaderboard)


    results_filename = '../results/auto_runner_resultsBatch.csv'
    df = pd.DataFrame(columns=['args','Global score','Final score', 'Tie Breaking score'])
    df.loc[0]= [args, global_corr_test_avg, final_score_test_avg, tiebreaker_score_test_avg]
    if os.path.exists(results_filename):
        df2 = pd.read_csv(results_filename, sep=';')
        df = df2.append(df)
    df.to_csv(results_filename, header=True, index = False, sep=';')


    return final_score_test_avg

#------------------------------------------------

def run_fold(params):

        X_train = params['X_train']
        X_test = params['X_test']
        y_train = params['y_train']
        y_test = params['y_test']
        info_train = params['info_train']
        info_test = params['info_test']
        multiindex = params['multiindex']
        args = params['args']
        loadSavedModel =  params['loadSavedModel']

        # data_params = args['data_params']
        model_params = args['model_params']
        # pre_params = args['pre_params']
        logging.info("CV size: %s", X_train.shape)
        model = get_model(model_params)



        # if loadSavedModel is None:
        #     logging.info('fitting model started ....')
        #     if model_params['type']in ['nn_pipeline_custom']:
        #         model.fit(X_train, y_train, X_test, y_test) # used to calc validation error during the training
        #     else:
        #          model.fit(X_train, y_train)
        #     logging.info('model fitting finished')
        #
        # else:
        #     logging.info("Loading Saved model from: %s", loadSavedModel)
        #     model.load_weights(X_train, loadSavedModel)

        if loadSavedModel is None:
            logging.info('fitting model started ....')
            if model_params['type']in ['nn_pipeline_custom']:
                model.fit(X_train, y_train, X_test, y_test) # used to calc validation error during the training
            else:
                 model.fit(X_train, y_train)
            logging.info('model fitting finished')

        else:
            logging.info("Loading Saved model from: %s", loadSavedModel)
            model.fit(X_train, y_train, X_test, y_test, loadSavedModel)

        # test score
        logging.info('prediction started ...')
        pred_test_value = model.predict(X_test)

        pred_test_value = np.nan_to_num(pred_test_value)
        obs_test = get_df(info_test, 'SYNERGY_SCORE', y_test)
        pred_test = get_df(info_test, 'PREDICTION', pred_test_value)

        global_scores, final_score, tiebreaker_score = getAllScores_ch1(obs_test, pred_test)

        # train scores
        pred_train_value = model.predict(X_train)
        pred_train_value = np.nan_to_num(pred_train_value)
        obs_train = get_df(info_train, 'SYNERGY_SCORE', y_train)
        pred_train = get_df(info_train, 'PREDICTION', pred_train_value)
        global_scores_train, final_scores_train, tiebreaker_score_train  = getAllScores_ch1(obs_train, pred_train)

        logging.info('Testing global score = %f' % global_scores)
        logging.info('Testing Final score = %f' % final_score)
        logging.info('Testing tiebreaker score = %f' % tiebreaker_score)

        logging.info('Training global score = %f' % global_scores_train)
        logging.info('Training Final score = %f' % final_scores_train)
        logging.info('Training tiebreaker score = %f' % tiebreaker_score_train)
        scores = global_scores, final_score, tiebreaker_score, global_scores_train, final_scores_train, tiebreaker_score_train
        predictions = pred_test, pred_train
        return scores, predictions

def run_cv(data_random_state, args,  n_folds = 3, parallel=False, data_phase = phase.training):
    '''
        Returns all avaivale scoring methods
        Global and mean correlation, + what will be available in round4
    '''
    logging.info("Running new run_cv\n========================\n")
    # np.random.seed(130481) # need for multiprocessing
    # np.random.seed() # need for multiprocessing

    data_params = args['data_params']
    model_params = args['model_params']
    pre_params = args['pre_params']
    logging.info(model_params)
    logging.info(pre_params)
    # get data

    if model_params['type'] in ['multikernel' , 'nn_pipeline' ,'multimodels' ,'bioNN', 'nn_pipeline_custom']:
        multiindex =True
    else:
        multiindex =False

    # multiindex =True
    print ('multiindex', multiindex)
    X, y, info = prepare_inputs(data_params, phase.training, multiindex=multiindex)

    if data_phase == phase.leaderboard:
        X_lb, y_lb, info_lb = prepare_inputs(data_params, phase.leaderboard, multiindex=multiindex)
        info = pd.concat((info,info_lb ), axis=0)
        if multiindex:
            X =  pd.concat((X, X_lb), axis=0)
            y =  np.concatenate((y, y_lb), axis=0)
        else:
            X =  np.concatenate((X, X_lb), axis=0)
            y =  np.concatenate((y, y_lb), axis=0)

    labels = info['COMBINATION_ID']
    n_folds = n_folds
    cv = StratifiedKFold(labels, n_folds,random_state= data_random_state, shuffle=True)

    global_scores_list, final_score_list, tiebreaker_score_list, global_scores_train_list, final_scores_train_list, tiebreaker_score_train_list, inputs = [],[],[],[],[],[],[]

    for i, (train_idx, test_idx) in enumerate(cv):

        # model = get_model(model_params)

        if multiindex:
            X_train, X_test = X.iloc[train_idx,:], X.iloc[test_idx,:]
        else:
            X_train, X_test = X[train_idx,:], X[test_idx,:]

        y_train, y_test = y[train_idx], y[test_idx]
        logging.info("CV size: %s, %s", y_train.shape, y_test.shape)
        #remove y outliers
        y_train = pre.remove_outliers(y_train)

        info_train, info_test = info.iloc[train_idx,:], info.iloc[test_idx,:]
        inn = {'X_train': X_train,'X_test': X_test,'y_train':y_train, 'y_test': y_test,'info_train': info_train,'info_test': info_test, 'multiindex': multiindex ,'args': args, 'loadSavedModel': None}
        inputs.append(inn)

    if parallel:
        pool = multiprocessing.Pool(n_folds)
        print 'len of inputs', len(inputs)
        results = pool.map(run_fold,inputs )
    else:
        results = map(run_fold,inputs)


    for r in results:
        scores= r[0]
        predictiosn = r[1]
        global_scores, final_score, tiebreaker_score, global_scores_train, final_scores_train, tiebreaker_score_train = scores

        global_scores_list.append(global_scores)
        final_score_list.append(final_score)
        tiebreaker_score_list.append(tiebreaker_score)

        global_scores_train_list.append(global_scores_train)
        final_scores_train_list.append(final_scores_train)
        tiebreaker_score_train_list.append(tiebreaker_score_train)

    global_corr_test_avg  = np.mean(global_scores_list)
    final_score_test_avg = np.mean(final_score_list)
    tiebreaker_score_test_avg = np.mean(tiebreaker_score_list)

    logging.info('-------------------------------------')
    logging.info('Done with %d folds',n_folds)
    logging.info('Global (avg): %s', global_corr_test_avg )
    logging.info('Final* (avg): %s', final_score_test_avg )
    logging.info('Tiebreak (avg): %s', tiebreaker_score_test_avg )

    logging.info('Training Global (avg): %s', np.mean(global_scores_train_list) )
    logging.info('Training Final* (avg): %s', np.mean(final_scores_train_list) )
    logging.info('Training Tiebreak (avg): %s', np.mean(tiebreaker_score_train_list) )

    df = pd.DataFrame(columns=['Global','Final*', 'Tiebreak','TrGlobal', 'TrFinal','TrTiebreak']  )
    df.loc[0]= [global_corr_test_avg, final_score_test_avg, tiebreaker_score_test_avg, np.mean(global_scores_train_list), np.mean(final_scores_train_list), np.mean(tiebreaker_score_train_list)]
    logging.info('\n %s',df)
    logging.info('random state')
    # random_state = np.random.get_state()
    # logging.info(random_state)

    return -global_corr_test_avg, -final_score_test_avg, -tiebreaker_score_test_avg

# -----------------------------------------------------------------

