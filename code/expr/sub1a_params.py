

# --------------------  multimodel Random Forest -------------------------------/
# base_models_AE = {
#     'params': {'max_features': 'auto', 'splitter': 'best', 'max_depth': 70, 'criterion': 'friedman_mse'},
#     'type': 'randomforest',
#     'bag': {'n_estimators': 50, 'n_jobs': 3}
# }
#
# base_models_org = {
#     'params': {'max_features': 'auto', 'splitter': 'best', 'max_depth': 70, 'criterion': 'friedman_mse'},
#     'type': 'randomforest',
#     'bag': {'n_estimators': 50, 'n_jobs': 3}
# }
#
# models_sub1A = {'mu':base_models_org, 'mut_AE': base_models_AE, 'cnv':base_models_org, 'cnv_AE': base_models_AE,
#                 'ge': base_models_org, 'ge_AE': base_models_AE, 'meth':base_models_org, 'meth_AE': base_models_AE,}
# model_params_multi = {'type': 'multimodels', 'params': {'models': models_sub1A }}

# ---------------------------------------------------------

pre_params = {'type': 'standard'}

data_params =  {
                'subchallenge': 'A',
                'add_tissues': True,       # if False, no tissue info is used
                'drug_features': {
                            'subchallenge': 'A',
                            'targets': True,
                            'drug_features': True,
                            'combination_factor':False,
                            'processed_targets': True,
                            'drug_info': False,
                            'log_scores': False,
                            'challenge':1,
                            'drug_auto' : None
                        },
                'pca_path' : 'pca/no_filter',
                # 'pca_path' : 'pca/full_drug_pathways'

                # 'auto_path': 'auto/full_drug_pathways/',
                'auto_path': 'auto/no_filter',



                'load_meyth': False, 'meth_auto' : True, 'meth_pca' : True,
                'meyth_aggr':  'beta_corr_tissue', # corr with ge, by tissue type #------> best
                                # 'beta_sum',     # no imputation, no corr, sum per gene
                'load_ge':  False, 'ge_auto':True, 'ge_pca' :True,
                'ge_impute': True,
                'load_cnv': False, 'cnv_auto':True, 'cnv_pca':True,
                'load_mut': False, 'mut_auto':True, 'mut_pca' : True,
                'mut_filter': None,
                'cnv_filter': None,

                'mut_agg_type': 1,  # 'raw': all possible features, numeric
                                    # 0: no aggregation, relevant features
                                    # 1: 1 feature per gene                # -----> Best
                'cnv_agg_type': 0,
                                    # 'raw: zygosity as categorical (6 features)
                                    # 0: zygosity as numeric (4 features)    #------> Best
                                    # 1: aggregation into one number
                'prior_integration': 'filtering',
                                     # None,  # no filtering of molecular data

                'pathways': [                       #------> Best
                                'pathways_cancer',
                                'minoacyl-tRNA',
                                'NF-kappaB',
                                'cell_cycle',
                                'transforming_growth_factor-beta',
                                'apoptosis',
                                'p53_signaling',
                                'MAPK_signaling',
                                'PI3K-Akt_signaling',
                                'Wnt_signaling',
                                'mTOR_signaling'
                              ]
               }


def get_model1a():
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.1), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,), 'epoch': 300, 'rmsp': True, 'lr': 0.001, 'architecture': (('ge_AE', 'drugs'), ('cnv_AE', 'drugs'), ('meth_AE', 'drugs'), ('mut_AE', 'drugs'), ('drugs', 'tissue_type')), 'more_dropouts': (0.4, 0.4), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}
    
    pre_params = {'type': 'smart', 'params': {'type': 'standard'}}
    data_params =  {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'), 'cnv_pca': False, 'prior_integration': 'filtering', 'ge_pca': False, 'scaling_factor': 100, 'load_cnv': False, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': True, 'pca_path': 'pca/no_filter', 'load_mut': False, 'tranform_by_DT': False, 'ge_auto': True, 'mut_pca': False, 'meth_pca': False, 'load_ge': False, 'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'A', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'A'}
 
    return model_params, pre_params, data_params


def get_model1a_smart_pca():    #------> best
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.1), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.2,), 'dataype_nhids': (100,), 'epoch': 300, 'rmsp': True, 'lr': 0.001, 'architecture': (('ge_AE', 'drugs'), ('cnv_AE', 'drugs'), ('meth_AE', 'drugs'), ('mut_AE', 'drugs'),('mut_PCA','cnv_PCA','meth_pca','ge_PCA', 'drugs'), ('drugs', 'tissue_type')), 'more_dropouts': (0.4, 0.4), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}
    
    pre_params = {'type': 'smart', 'params': {'type': 'standard'}}
    data_params =  {
                'subchallenge': 'A',
                'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'),
                'cnv_pca': True, 'prior_integration': 'filtering', 'ge_pca': True, 'scaling_factor': 100, 'load_cnv': False, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': True, 'pca_path': 'pca/no_filter', 'load_mut': False, 'tranform_by_DT': False, 'ge_auto': True,
                'mut_pca': True, 'meth_pca': True, 'load_ge': False,
                'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'A', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True}
 
    return model_params, pre_params, data_params


def get_model1a_smart_pca_highdrop():
    model_params = {'params': {'verbose': 2, 'activation': 'sigmoid', 'saveModels': None, 'batch_size': 10, 'more_regularization': (0.001, 0.1), 'more_nhids': (200, 100), 'early_stopping': None, 'datatype_regularization': (0.001,), 'decay': 1e-06, 'merger': 'concat', 'datatype_dropouts': (0.4,), 'dataype_nhids': (100,), 'epoch': 300, 'rmsp': True, 'lr': 0.001, 'architecture': (('ge_AE', 'drugs'), ('cnv_AE', 'drugs'), ('meth_AE', 'drugs'), ('mut_AE', 'drugs'),('mut_PCA','cnv_PCA','meth_pca','ge_PCA', 'drugs'), ('drugs', 'tissue_type')), 'more_dropouts': (0.5, 0.5), 'account_for_drugs': True, 'momentum': 0.9, 'preprocessors': ({'params': {'feature_range': (-1, 1)}, 'type': 'scale'},)}, 'type': 'nn_pipeline_custom'}
    
    pre_params = {'type': 'smart', 'params': {'type': 'standard'}}
    data_params =  {'load_meyth': False, 'mut_auto': True, 'cnv_agg_type': 0, 'auto_path': 'auto/no_filter', 'cnv_filter': None, 'meyth_aggr': 'beta_corr_tissue', 'mut_filter': None, 'pathways': ('pathways_cancer', 'minoacyl-tRNA', 'NF-kappaB', 'cell_cycle', 'transforming_growth_factor-beta', 'apoptosis', 'p53_signaling', 'MAPK_signaling', 'PI3K-Akt_signaling', 'Wnt_signaling', 'mTOR_signaling'),
            'cnv_pca': True, 'prior_integration': 'filtering', 'ge_pca': True, 'scaling_factor': 100, 'load_cnv': False, 'mut_agg_type': 1, 'add_tissues': True, 'cnv_auto': True, 'meth_auto': True, 'pca_path': 'pca/no_filter', 'load_mut': False, 'tranform_by_DT': False, 'ge_auto': True,
            'mut_pca': True, 'meth_pca': True, 'load_ge': False,
            'drug_features': {'drug_auto': None, 'log_scores': False, 'drug_info': False, 'challenge': 1, 'combination_factor': False, 'subchallenge': 'A', 'processed_targets': True, 'drug_features': True, 'targets': True}, 'ge_impute': True, 'subchallenge': 'A'}
 
    return model_params, pre_params, data_params



# ----------------- Choose model --------------------------

model_params, pre_params, data_params = get_model1a_smart_pca()
