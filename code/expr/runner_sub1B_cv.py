from functools import partial

__author__ = 'haitham'

from dataLoader.data_low_level_clean import phase
from train_predict import run, run_cv
import numpy as np
import datetime
import logging
import sys
import os
import time
import multiprocessing
import random

from sub1b_params import model_params, pre_params, data_params


def log_results(n_iters,results, run='3CV'):
    global_score_avg, finalScore_avg, tiebreaker_avg, random_states = [], [], [], []
    for i in range(n_iters):
        global_score_avg.append(results[i][0])
        finalScore_avg.append(results[i][1])
        tiebreaker_avg.append(results[i][2])
        # random_states.append(results[i][3])


    if n_iters >=1:
        logging.info('final %s', finalScore_avg)
        logging.info('tie %s', tiebreaker_avg)
        logging.info('-----------------------%s',run)
        logging.info("Average of %d iterations: mean (std) \n********************" % n_iters)
        logging.info("Global score (old):  \t %f \t  (%f) "% (np.mean(global_score_avg), np.std(global_score_avg)))
        logging.info("Final score ***:  \t \t  %f \t (%f) "% (np.mean(finalScore_avg), np.std(finalScore_avg)))
        logging.info("Tie Breaking score: \t  %f \t (%f) "% (np.mean(tiebreaker_avg), np.std(tiebreaker_avg)))


        logging.info(details)

        return global_score_avg, finalScore_avg, tiebreaker_avg


def run_range_of_seeds(n_iters =10, n_jobs = 2):
    # NN
    # rnds = [925371, 911787, 971741, 960542, 966979, 924399, 966710, 905731, 981411, 897438]
    # results_cv =  [[-0.21509945], [-0.23452473], [-0.21436963], [-0.21892793], [-0.1948875], [-0.2314115333], [-0.19329], [-0.21196], [-0.220529], [-0.21781]]
    # results_run = [[-0.2567786], [-0.2698359], [-0.2154541], [-0.2508436], [-0.2688607], [-0.2539447], [-0.2395999], [-0.2389205], [-0.2685816], [-0.1946593]]

    rnds = random.sample(range( 1, 8802008), n_iters)

    # print rnds
    logging.info('random seeds')
    logging.info(rnds)

    pool = multiprocessing.Pool(n_jobs)
    results_run = pool.map(_run, rnds)

    logging.info('random seeds')
    logging.info(rnds)
    logging.info('results LB RUN')
    logging.info(results_run)

    best = np.min(results_run )
    index = np.argmin(results_run)
    best_seed = rnds[index]
    logging.info('best LB score is %s', best)
    logging.info('best seed for LB score is %s', best_seed)
    return best_seed


def _run_cv(random_state ,n_iters = 10, n_job=10):
    np.random.seed(random_state)

    data_phase =  phase.leaderboard #  phase.leaderboard | phase.training
    number_of_processes = n_job # (1== no parallelization ), select it based on the number of cores that you have on your machine
    # -----
    rnd_seeds = [5878595, 7667993, 3373553, 6178984, 3795226, 2970918, 8516676, 3867199, 1878007, 7994439, 7345389, 4073748, 974778, 7908908, 580363, 5536697, 8413843, 7328635, 1033563, 5895755, 4286564, 3062525, 6722291, 7882627, 1781564, 7509791, 4000532, 8579830, 2262355, 6030533, 58520, 5369686, 332487, 5872704, 5255914, 6603677, 4185383, 8500737, 1811017, 5378767, 6612491, 6945004, 7382438, 6607690, 6503605, 505822, 4678901, 2561988, 915664, 4547059, 1173529, 6804906, 1647993, 5629650, 221735, 1800640, 5459636, 8133554, 2994772, 4078164, 5861360, 2638867, 3448509, 3578616, 3672090, 3920640, 7659407, 3437919, 3646311, 3122627, 3590172, 8247414, 6775362, 4310826, 561419, 102597, 8543738, 2966981, 1501694, 5710025, 4068214, 1706686, 520990, 1792132, 2912887, 1883412, 4168676, 7268396, 7471756, 1329136, 1357076, 1144392, 5625659, 6738366, 1042543, 1789172, 4299677, 2209792, 5253201, 6858978]
    data_rnds = rnd_seeds[0:n_iters]

    # args_list= [args]*n_iters
    start = time.time()
    if number_of_processes >1:
        # parallel
        run_cv_part = partial(run_cv, args=args, parallel=False, data_phase = data_phase) # no nested parallelization is allowed
        pool = multiprocessing.Pool(number_of_processes)
        results= pool.map(run_cv_part, data_rnds) # start running in parallel
    else:
        # sequential
        run_cv_part = partial(run_cv,args=args, parallel=False,  data_phase = data_phase) # change to True if you want to run the 3 folds in parallel
        results= map(run_cv_part, data_rnds)
    endd = time.time() - start
    logging.info('elapsed time %f mins'% (endd/60))
    global_score_avg, finalScore_avg, tiebreaker_avg = log_results(n_iters,results)
    return  finalScore_avg


def _run(random_state):
    np.random.seed(random_state)
    n_iters = 1
    data_phase =  phase.leaderboard #  phase.leaderboard | phase.training
    number_of_processes = 1 # (1== no parallelization ), select it based on the number of cores that you have on your machine

    logging.info(details)
    args_list= [args]*n_iters
    save_path = '../results/BestRuns/test/'
    run_part = partial(run, data_phase=data_phase, save_path= save_path)
    # run_part = partial(run, data_phase=data_phase, save_path= save_path, args= args )

    start = time.time()
    if number_of_processes >1:
       # parallel
       pool = multiprocessing.Pool(number_of_processes)
       results= pool.map(run_part, args_list) # start running in parallel
    else:
       # sequential
       results= map(run_part, args_list)
    endd = time.time() - start
    logging.info('elapsed time %f mins'% (endd/60))


    global_score_avg, finalScore_avg, tiebreaker_avg = log_results(n_iters,results,'leaderboard')
    return finalScore_avg


# setup logging
base_folder ='../logs'
timeStamp = '_{0:%b}-{0:%d}_{0:%H}-{0:%M}'.format(datetime.datetime.now())
filename= os.path.basename(__file__)
filename, file_extension = os.path.splitext(filename)
filename = os.path.join(base_folder,filename+ timeStamp+'.log')
print 'Log file: ', filename
logging.basicConfig(filename = filename,
                    filemode='w',
                    format='%(asctime)s - {%(filename)s:%(lineno)d} - %(message)s',
                    datefmt='%m/%d %I:%M',
                    level=logging.INFO) # or logging.DEBUG
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

details = 'sub1B crossvalidation'

logging.info(details)

args = {}
args['data_params'] = data_params
args['model_params'] = model_params
args['pre_params'] = pre_params
logging.info("Args: %s", args)

best_seed = run_range_of_seeds(n_iters=15, n_jobs=15)

random_seed= best_seed
# random_seed= 187702
np.random.seed(random_seed)
logging.info('random seed')
logging.info(random_seed)

_run_cv(random_seed ,n_iters = 10, n_job=10)


