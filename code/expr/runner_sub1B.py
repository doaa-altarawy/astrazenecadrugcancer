__author__ = 'haitham'

from dataLoader.data_low_level_clean import phase
from dataLoader.combin_priority import generate_combin_priority
from train_predict import run
import numpy as np
import datetime
import logging
import sys
import os

from sub1b_params import model_params, pre_params, data_params


# ---------------------------------------------------------------------------


# setup logging
base_folder ='../logs'
timeStamp = '_{0:%b}-{0:%d}_{0:%H}-{0:%M}'.format(datetime.datetime.now())
filename= os.path.basename(__file__)
filename, file_extension = os.path.splitext(filename)
filename = os.path.join(base_folder,filename+ timeStamp+'.log')
print filename
logging.basicConfig(filename = filename,
                    filemode='w',
                    format='%(asctime)s - {%(filename)s:%(lineno)d} - %(message)s',
                    datefmt='%m/%d %I:%M',
                    level=logging.INFO) # or logging.DEBUG
logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))


details = 'sub1B final submission'
logging.info(details)
random_seed= 7703283
np.random.seed(random_seed)
logging.info('random seed')
logging.info(random_seed)

args = {}
args['data_params'] = data_params
args['model_params'] = model_params
args['pre_params'] = pre_params
logging.info("Args: %s", args)

#-------------------   generate submission  ----------------#

save_path = '../../results/sub1b/'
logging.info('Generating submission file in %s', save_path)

#---> Score leaderboard
global_scores, final_score, tiebreaker_score = run(args, data_phase=phase.leaderboard, save_path= save_path)

#---> Generate submission for test data
run(args, data_phase=phase.test, save_path= save_path)

generate_combin_priority()
logging.info("VTForce2")
logging.info('Submission file saved in %s', save_path)
logging.info("Leaderboard scores: ")
logging.info("Primary Metric: %s", -final_score)
logging.info("Tie Breaking: %s", -tiebreaker_score)
logging.info('Tissue_type used=%s', data_params['add_tissues'])