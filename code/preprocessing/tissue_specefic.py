__author__ = 'Delasa'
import numpy as np
import pandas as pd
from scipy import stats
from dataLoader.dataLowLevel import load_tissues

#cdf = np.vectorize(stats.norm.cdf)

class tissue_specific(object):
    def __init__(self, with_centering=True, with_cdfing=True, copy=True):
        self.with_centering = with_centering
        self.with_cdfing = with_cdfing
        self.copy = copy
        tissue_data = load_tissues()
        tissues = tissue_data['Tissue..General.'].unique()
        self.tissues = tissues
        self.tissue_data = tissue_data

    def fit(self, X_train, info_X, Y):
        #TODO: you can check the dimension to see if they agree
        pass

    def transform(self, X, info_X, Y):
         return self.predict(X, info_X, Y)


    def predict(self, X, info_X, Y):
        print 'Entering tissue specific normalization ...'
        # output_X = []
        # output_Y = []
        # info_X_output = []

        tissues = self.tissues
        tissue_data = self.tissue_data
        info_X_reset = info_X.reset_index()
        # info_X_colnames = info_X.columns    # 3 cols
        info_tissues = pd.merge(info_X_reset, tissue_data, left_on='CELL_LINE', right_index=True, how='inner')
        for this_tissue in tissues:
                print this_tissue
                this_info_tissue = info_tissues[info_tissues['Tissue..General.'] == this_tissue]
                this_tissue_index = this_info_tissue.index.values
                # this_tissue_data = X[this_tissue_index, :]
                # o_X  = this_tissue_data


                if self.with_centering:
                    X[this_tissue_index, :] = stats.zscore(X[this_tissue_index, :], axis=1)

                if self.with_cdfing:
                    X[this_tissue_index, :] = np.apply_over_axes(stats.norm.cdf, X[this_tissue_index, :], [0,1])


                # output_X.append(o_X)

                #if self.with_cdfing:
                #    o_X = stats.norm.cdf(o_X,axis=1)

                #---- update into_X
                # info_X_output.append(info_X.iloc[this_tissue_index, :])

                # if Y is not None:   # in case when generating predictions, no Y
                #     o_Y  = Y[this_tissue_index]
                #     output_Y.append(o_Y)
                #     print o_Y.shape

                # #print type(o_Y)
                # print o_X.shape
                # print 'processing ', this_tissue, ' finished'


        # info_X = np.concatenate(info_X_output, axis=0)
        # info_X = pd.DataFrame(info_X, columns=info_X_colnames)
        # info_X.set_index('index', inplace=True)
        # print 'info shape: ', info_X.shape

        # output_X = np.vstack(output_X)
        # if len(output_Y)>0:
        #     output_Y = np.concatenate(output_Y, axis=0)
        # print 'tissue specific preprocessing is done'
        # print output_X.shape#, output_Y.shape
        # print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        return X, Y

