__author__ = 'marakeby'
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, ActivityRegularization, AutoEncoder
from keras.optimizers import SGD, RMSprop, rmsprop
import numpy as np



# def feature_extraction(X_train, X_test):
#     X_train2= np.copy(X_train)
#     model = Sequential()
#     auto = get_autoencoder(200)
#     model.add(auto)
#     sgd = SGD(lr=0.000001, decay=1e-6, momentum=0.9, nesterov=False)
#     model.compile(loss='mean_squared_error', optimizer=sgd)
#     model.fit(X_train2, X_train2,nb_epoch=100, batch_size=10, verbose=1 )
#     xtrain = model.predict(X_train2)
#     xtest = model.predict(X_test)
#     return xtrain, xtest

class Auto(object):
    def __init__(self,  lr= 0.05, epoch=3, batch_size= 10, momentum=.9, activation = 'tanh', decay=1e-6, aut_epoch = 3, auto_nhids= [100, 50, 20] , auto_lr = 0.05):
        # self.lr = 0.01
        # self.auto_epoch= 100
        # # self.epoch= 10
        # self.batch_size= 20
        # self.momentum =.90
        # self.decay = 1e-6
        # self.auto_nhids= [  200,100]

        self.nb_epoch = epoch
        self.batch_size= batch_size
        self.activation = activation #'relu', tanh, sigmoid

        self.auto_nhids= auto_nhids
        self.auto_epoch = aut_epoch
        self.lr = lr
        self.momentum = momentum
        self.decay = decay
        self.auto_lr = auto_lr


    def fit(self, X_train):
        n = X_train.shape[1]
        model = Sequential()
        auto = self.get_auto(X_train)
        # auto = self.get_auto(X_train)

        # model.add(auto)
        # sgd = SGD(lr=self.lr, decay= self.decay, momentum=self.momentum, nesterov=False)
        # model.compile(loss='mean_squared_error', optimizer=sgd)
        # model.compile(loss='mean_squared_error', optimizer=RMSprop())
        # model.fit(X_train, X_train,nb_epoch=self.epoch, batch_size=self.patch_size, verbose=1 )
        self.model = auto

    def transform(self, X):
         return self.predict(X)

    def fit_transform(self, X):
        self.fit(X)
        return self.transform(X)

    def predict(self, X):
        output = [X]
        input = X
        for encoder in self.model:
                o = encoder.predict(input)
                print o.shape
                output.append( o)
                input = o
        output = np.hstack(output)
        return output

    def get_auto(self, X_train):
        # Layer-wise pre-training
        trained_encoders = []
        X_train_tmp = X_train
        nb_hidden_layers=[]
        nb_hidden_layers.append( X_train.shape[1])
        nb_hidden_layers.extend( self.auto_nhids)
        # nb_hidden_layers.extend( [200, 100, 50])
        print X_train_tmp.shape
        print nb_hidden_layers
        layers_output =[]
        for n_in, n_out in zip(nb_hidden_layers[:-1], nb_hidden_layers[1:]):
            print('Pre-training the layer: Input {} -> Output {}'.format(n_in, n_out))
            # Create AE and training
            ae = Sequential()
            encoder = Sequential([Dense(input_dim = n_in, output_dim = n_out, activation=self.activation)])
            decoder = Sequential([Dense(input_dim = n_out, output_dim = n_in, activation= self.activation)])
            ae.add(AutoEncoder(encoder=encoder, decoder=decoder, output_reconstruction=False))
            # opt = rmsprop(lr = 0.3,  epsilon=1e-9)
            opt = SGD(lr=self.auto_lr, decay= self.decay, momentum= self.momentum, nesterov=False)
            ae.compile(loss='mean_squared_error', optimizer=opt)
            # ae.fit(X_train_tmp, X_train_tmp, batch_size=self.batch_size, nb_epoch=self.nb_epoch, verbose=0)
            ae.fit(X_train_tmp, X_train_tmp, batch_size=self.batch_size, nb_epoch= self.auto_epoch, verbose=1)
            # Store trainined weight
            # trained_encoders.append(ae.layers[0].encoder)
            trained_encoders.append(ae)
            # Update training data
            X_train_tmp = ae.predict(X_train_tmp)
            # layers_output.append(X_train_tmp)
        return trained_encoders
