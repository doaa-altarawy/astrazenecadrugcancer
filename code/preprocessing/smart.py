from sklearn import preprocessing as p
import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator

proc =  p.StandardScaler()

class SmartPreprocesor(BaseEstimator):
   def __init__(self, numrical_processor=p.StandardScaler() , binary_scaler=None):
       self.numrical_processor = numrical_processor
       print self.numrical_processor

   def fit(self, X_train, **fit_params):
       print 'fit_params', fit_params
       #expects dataframe
       # x = x.convert_objects(convert_numeric=True)
       # self.X_train = X_train.convert_objects(convert_numeric=True)
       self.X_train  = X_train
       self.numerical_cols = []
       self.numerical_cols_ix = []
       self.bool_cols = []
       self.bool_cols_ix = []
       self.cat_cols = []
       self.cat_cols_ix = []
       self.other_cols = []
       self.other_cols_ix = []
       counts =[]

       for i, c in enumerate(self.X_train.columns):
           # print c
           count = len(pd.unique(self.X_train[c].ravel()))
           counts.append(count)
           if count<2:
               self.other_cols.append(c)
               self.other_cols_ix.append(i)
           if count==2:
               self.bool_cols.append(c)
               self.bool_cols_ix.append(i)
           elif count >2 and count < 10:
               self.cat_cols.append(c)
               self.cat_cols_ix.append(i)
           if count >= 10:
               self.numerical_cols.append(c)
               self.numerical_cols_ix.append(i)


       # numericals = X_train[self.numerical_cols].as_matrix()
       x_matrix  = self.X_train.as_matrix()
       numericals = x_matrix[:, self.numerical_cols_ix]

       # cat =  X_train[self.cat_cols]
       # cat = pd.get_dummies(cat)

       n_num = len(self.numerical_cols)
       n_bool= len(self.bool_cols)
       n_cat = len(self.cat_cols)
       n_other = len(self.other_cols)
       total = n_num + n_bool+ n_cat +n_other
       print('numerical cols %d, bolean cols %d, categorical cols %d, other cols %d,  total cols %d'% (n_num, n_bool, n_cat,n_other,  total))


       self.numrical_processor.fit(numericals)
       return self

   def transform(self, X):
        return self.predict(X)

   def fit_transform(self, X, y=None, copy=None):
       self.fit(X)
       return self.transform(X)

   def predict(self, X):
       print 'transforimg'
       # X = X.convert_objects(convert_numeric=True)
       # ret  = X.as_matrix()
       # numericals = X[self.numerical_cols].as_matrix()
       ret = []

       if len(self.numerical_cols) >0:
           numericals = X[self.numerical_cols].as_matrix()
           numericals = self.numrical_processor.transform(numericals)
           ret.append(numericals)
       if len(self.bool_cols) > 0:
           bool = X[self.bool_cols].as_matrix()
           ret.append(bool)
       if len( self.cat_cols) > 0:
           cat = X[self.cat_cols]
           # cat = pd.get_dummies(cat)
           ret.append(cat.as_matrix())
       if len(self.other_cols) > 0:
           other = X[self.other_cols].as_matrix()
           ret.append(other)

       ret = np.hstack(ret)
       return  ret

   def get_params(self, deep=True):
       return {'numrical_processor': self.numrical_processor}


# http://stackoverflow.com/questions/24458645/label-encoding-across-multiple-columns-in-scikit-learn
