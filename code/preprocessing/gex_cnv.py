__author__ = 'Delasa'

import pandas as pd
from sklearn import linear_model
from math import sqrt

from dataLoader.dataLowLevel import phase, load_ge, load_cnv, processed_data_folder

cnv = load_cnv()
gex = load_ge()

cnv_genes = cnv.columns.get_level_values(0)
gex_genes = gex.columns.get_level_values(0)

common_genes = list(set.intersection(set(cnv_genes), set(gex_genes)))

cnv.fillna(0, inplace=True)
gex.fillna(0, inplace=True)

def fit_lasso(gene='RNF14'):
    #clf = linear_model.Lasso(alpha=0.1)
    clf =linear_model.LinearRegression()

    y = gex[gene]
    X = cnv[gene]

    common_cell_lines = list(set.intersection(set(y.index), set(X.index)))
    y = y[y.index.isin(common_cell_lines)]
    X = X[X.index.isin(common_cell_lines)]
    #print len(common_cell_lines)
    clf.fit(X, y)
    #print('clf.coef: %s' %clf.coef_)
    #print('clf.intercept_ %s' %clf.intercept_)
    R2 = clf.score(X,y)
    confidence = (1 - sqrt(1 - R2)) * 100
    #print ('R^2 %s' %R2)
    return R2, confidence

gene_df = pd.DataFrame(index=common_genes, columns=['R^2','confidence'])

for thisgene in common_genes:
    R2,confidence = fit_lasso(thisgene)
    gene_df.ix[thisgene]['R^2'] = R2
    gene_df.ix[thisgene]['confidence'] = confidence

#gene_df_filtered = gene_df[gene_df.R2 > 10]

#gene_df_filtered.to_csv(processed_data_folder + 'gex_cnv_Linear.txt', sep='\t', index_label='Gene')
gene_df.sort('R^2', ascending = False, inplace=True)
gene_df.to_csv(processed_data_folder + 'gex_cnv_Linear.txt', sep='\t', index_label='Gene')

