#Process Illumina data
id<-read.csv("../../data/Sanger_molecular_data/methyl/CpG_probe_level/probe_info.csv",header=T,check.names=F)
probe_id<-id$Name #sub('>[0-9]+_strand[0-9]_chrom','',multi_read_best_id)
gene_id<-sub("*;.+","",id$UCSC_RefGene_Name) 
#read met.csv and get genes
met<-read.csv("../../data/Sanger_molecular_data/methyl/CpG_probe_level/methyl_probe_beta.csv",header=T,check.names=F)
met_probe_id<-as.vector(met[,1])
index<-match(met_probe_id,probe_id)
met[,dim(met)[2]+1]<-gene_id[index]
notno<-which(gene_id[index]!="")
met<-met[notno,]
#get sum for all probes in each gene
gene<-as.vector(unique(met[,84]))
l=length(gene)
mat<-matrix(0,ncol=82,nrow=l)
for(i in c(1:l))
{
index<-which(met[,84]==gene[i])
subdat<-met[index,-c(1,84)]
mat[i,]<-colSums(subdat)
}	
#impute data "KMS-11","MDA-MB-175-VII","SW620"
avge<-rowMeans(mat)
add<-cbind(avge,avge,avge)
mat1<-cbind(gene,mat,add)
colnames(mat1)<-c("Gene.names",colnames(met)[-c(1,84)],"KMS-11","MDA-MB-175-VII","SW620")
write.csv(mat1,"../../processed_data/methyl/methyl_beta_sum.csv",row.names=F,quote=F)
