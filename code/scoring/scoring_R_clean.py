import os
import subprocess
from dataLoader.data_low_level_clean import get_config
__author__ = 'marakeby'
# import re
import numpy as np
import logging
import pandas as pd
import tempfile
import traceback


config = get_config()
print config['scoring_file']
scoring_file=  config['scoring_file']
Rscript_path = config['Rscript_path']


def score_submission(path):
    logging.info(path)
    obs_data = pd.read_csv('../../data/Drug_synergy_data/ch1_LB.csv')
    obs_data = obs_data.loc[:, ['CELL_LINE', 'COMBINATION_ID', 'SYNERGY_SCORE']]


    pred_data = pd.read_csv(path)
    _, final_score, tiebreaker_score  = getAllScores_ch1(obs_data, pred_data)
    logging.info("******* LeaderBoard score: Final score: %s, Tie score: %s",
                    final_score, tiebreaker_score)
    return final_score, tiebreaker_score



def getAllScores_ch1(obs, pred):


    obs_temp = tempfile.NamedTemporaryFile()
    pred_temp = tempfile.NamedTemporaryFile()

    obs.to_csv(obs_temp.name, index=False)
    pred.to_csv(pred_temp.name, index=False)

    path = os.path.dirname(os.path.realpath(__file__))
    logging.info('Calling R scoring script..............................')
    process = subprocess.Popen ([Rscript_path,'getGlobalScore_ch1_script.R',scoring_file, obs_temp.name ,pred_temp.name ],cwd=path,  stdout=subprocess.PIPE , stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    # print stdout, stderr
    alls = stdout.split('\n')[1].split()
    # p = re.compile(ur'score \n([+-]?[0-9.]+)')
    # # p = re.compile(ur'score([+-]?[0-9.]+)')
    # all=  re.findall(p, stdout)
    # print 'all', all
    # score = float(all[0])
    obs_temp.close()
    pred_temp.close()
    # score = stdout
    try:
        globalScore = float(alls[0].strip())
        finalScore = float(alls[1].strip())
        tieBrkScore = float(alls[2].strip())
    except:
        globalScore = np.NaN
        finalScore = np.NaN
        tieBrkScore = np.NaN

    return globalScore, finalScore, tieBrkScore


# def getDrugCombiScore_ch1(obs, pred, confidence='combination_priority_hong_mod.csv', topX=100):
#
#     obs_temp = tempfile.NamedTemporaryFile()
#     pred_temp = tempfile.NamedTemporaryFile()
#     confidence_temp = tempfile.NamedTemporaryFile()
#
#     obs.to_csv(obs_temp.name, index=False)
#     pred.to_csv(pred_temp.name, index=False)
#
#
#     path = os.path.dirname(os.path.realpath(__file__))
#     params = ['Rscript','getDrugCombiScore_ch1_script.R',scoring_file, obs_temp.name ,pred_temp.name]
#     # if not confidence is np.NAN:
#     #     confidence.to_csv(confidence_temp.name, index=False)
#     #     params.append(confidence_temp.name)
#     #     params.append(str(topX))
#     # else: # use default
#     #     params.append('combination_priority_haitham.csv')
#     #     params.append(str(100))
#
#     params.append(confidence)
#     params.append(str(topX))
#
#     # print 'params', params
#     process = subprocess.Popen ( params,cwd=path,  stdout=subprocess.PIPE , stderr=subprocess.PIPE)
#     stdout, stderr = process.communicate()
#     # print stdout, stderr
#     alls = stdout.split('\n')
#     # print 'alls', alls[1]
#     mean, ste, n, global_mean = alls[1].split()
#     mean = float(mean)
#     ste = float(ste)
#     n = float(n)
#     global_mean = float(global_mean)
#     obs_temp.close()
#     pred_temp.close()
#     if not confidence is np.NAN:
#         confidence_temp.close()
#
#     return mean, ste, n, global_mean
#
#
# def getObs_ch1(ls):
#     pass