__author__ = 'Delasa'
import synapseclient
import os
import zipfile
import pandas as pd
from dataLoader.data_low_level_clean import config


syn = synapseclient.Synapse()

# Add log in user, pass here
syn.login()

data_folder = config['data']['path']

LB_syn_id = 'syn5699550'

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def download_challenge_data(entity_id='syn4923176', entity_name='Challenge Data', path=data_folder):

    print(entity_id, entity_name)
    query_results = syn.query('SELECT id, name FROM entity WHERE parentId=="%s"' %entity_id)
    totalNumberOfResults = query_results['totalNumberOfResults']
    results = query_results['results']

    if totalNumberOfResults != 0:
        for result in results:
            entity_id = result['entity.id']
            entity_name = result['entity.name']
            print(entity_id, entity_name)
            download_challenge_data(entity_id,entity_name , path + '/' + entity_name)
    else:
        if not os.path.exists(path):
             os.makedirs(path)
        syn.get(entity_id, downloadFile=True, downloadLocation=path, ifcollision="keep.local")

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def download_challenge_zipfiles(path=data_folder):

    drug_synergy_zipfile = 'syn4925536'
    molecular_data_zipfile =  'syn4925558'
    print 'Downloading drug synergy data .....'
    syn.get(drug_synergy_zipfile, downloadFile=True, downloadLocation=path, ifcollision="keep.local")
    print 'Downloading molecular data .....'
    syn.get(molecular_data_zipfile, downloadFile=True, downloadLocation=path, ifcollision="keep.local")
    print('Downloading done.')
    print('Start extracting .....')
    with zipfile.ZipFile(path + '/Sanger_molecular_data.zip', "r") as z:
        z.extractall(path)

    with zipfile.ZipFile(path + '/Drug_synergy_data.zip', "r") as z:
        z.extractall(path)
    print('Download and extrcation done!')

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
