__author__ = 'Doaa'

import pandas as pd
import numpy as np
from enum import Enum
import yaml
import os
import logging
from sklearn import preprocessing as p


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class phase(Enum):
    training = 1
    leaderboard = 2
    test = 3

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def get_config():
    f = open('../config.yaml')
    config = yaml.safe_load(f)
    f.close()
    return config

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Load config
config = get_config()
processed_data_folder = config['data']['processed']
molecular_data_folder = config['data']['molecular']
drug_data_path = config['data']['drug']
methyl_processed = config['data']['methyl_processed']

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_ge_auto(auto_path='auto'):
    logging.info('Loading AE gene expression data...')

    ge_file = os.path.join(processed_data_folder, auto_path, 'ge_auto.csv')

    logging.info('loading ' + ge_file)
    data = pd.read_csv(ge_file ,sep=',', index_col=0, header=0)

    data.index.name = 'CELL_LINE'
    logging.info('Gene expression data loaded, shape: %s', data.shape)
    logging.debug(data.head())

    return data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_ge_pca(pca_path='pca'):
    logging.info('Loading AE gene expression data...')

    ge_file = os.path.join(processed_data_folder, pca_path, 'ge_pca.csv')

    logging.info('loading ' + ge_file)
    data = pd.read_csv(ge_file ,sep=',', index_col=0, header=0)

    data.index.name = 'CELL_LINE'
    logging.info('Gene expression data loaded, shape: %s', data.shape)
    logging.debug(data.head())

    return data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_ge(ge_impute=True):
    logging.info('Loading gene expression data...')

    if ge_impute:
        ge_file = os.path.join(processed_data_folder, 'ge', 'gex_impute.csv')
    else:
        ge_file = molecular_data_folder + 'gex.csv'

    logging.info('loading ' + ge_file)
    data = pd.read_csv(ge_file ,sep=',', index_col=0, header=0)
    data = data.T
    data.index.name = 'CELL_LINE'
    logging.info('Gene expression data loaded, shape: %s', data.shape)
    logging.debug(data.head())

    return data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_mutation_auto(auto_path='auto'):

    mu_file = os.path.join(processed_data_folder, auto_path, 'mu_auto.csv')
    logging.info('loading mutation: '+ mu_file)
    mut_data = pd.read_csv(mu_file, sep=',', index_col=0, header=0)

    mut_data.index.name = 'CELL_LINE'

    logging.info('Mutation AE data loaded, shape: %s', mut_data.shape)
    logging.debug(mut_data.head())

    return mut_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_mutation_pca(auto_path='pca'):

    mu_file = os.path.join(processed_data_folder, auto_path, 'mu_pca.csv')
    logging.info('loading mutation: '+ mu_file)
    mut_data = pd.read_csv(mu_file, sep=',', index_col=0, header=0)

    mut_data.index.name = 'CELL_LINE'

    logging.info('Mutation PCA data loaded, shape: %s', mut_data.shape)
    logging.debug(mut_data.head())

    return mut_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_mutation(load_processed=True, mut_agg_type=0, mut_filter=None):

    if mut_agg_type == 'raw':
        filename = os.path.join(processed_data_folder, 'mut', 'mutations_raw.tsv')
    elif mut_agg_type == 0:
        filename = os.path.join(processed_data_folder, 'mut', 'mutations_moreFeatures.tsv')
    elif mut_agg_type == 1:
        filename = os.path.join(processed_data_folder, 'mut', 'mutations_moreFeatures_aggregated.tsv')

    if load_processed:
        logging.info("Loading Mutation file: "+filename)
        mut_data = pd.read_csv(filename, sep='\t', header=0, index_col=[0,1])
        mut_data = mut_data.unstack(level=-1)
    # process mut
    else:
        mut_data = process_mutation_all_cellines(aggr_type=mut_agg_type)
        mut_data.to_csv(filename, sep='\t')


    mut_data.index.name = 'CELL_LINE'

    logging.info('Mutation data loaded, shape: %s', mut_data.shape)
    logging.debug(mut_data.head())

    # Filter mut based on a list of genes in a given file (from corr with gex)
    if mut_filter:
        mut_data = filterData(mut_data, mut_filter, 'Mutation')

    return mut_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_methylation_auto(auto_path='auto'):

    logging.info('Loading AE methylation data...')

    meth_file = os.path.join(processed_data_folder, auto_path, 'meth_auto.csv')
    logging.info('loading '+ meth_file)
    meth_data = pd.read_csv(meth_file, sep=',', index_col=0, header=0)
    meth_data.index = meth_data.index.to_series().str.replace('.', '-')

    meth_data.index.name = 'CELL_LINE'
    logging.info('meth_auto shape: %s', meth_data.shape)
    return meth_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_methylation_pca(pca_path='pca'):

    logging.info('Loading PCA methylation data...')

    meth_file = os.path.join(processed_data_folder,pca_path, 'meth_pca.csv')
    logging.info('loading '+ meth_file)
    meth_data = pd.read_csv(meth_file, sep=',', index_col=0, header=0)
    meth_data.index = meth_data.index.to_series().str.replace('.', '-')

    meth_data.index.name = 'CELL_LINE'
    logging.info('meth_pca shape: %s', meth_data.shape)
    return meth_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_drug_auto(auto_path='auto'):

    logging.info('Loading AE drug data...')

    drug_file = os.path.join(processed_data_folder,auto_path, 'drug_auto.csv')
    logging.info('loading '+ drug_file)
    meth_data = pd.read_csv(drug_file, sep=',', index_col=0, header=0)
    meth_data.index = meth_data.index.to_series().str.replace('.', '-')

    meth_data.index.name = 'CELL_LINE'
    logging.info('drug_auto shape: %s', meth_data.shape)
    return meth_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_methylation(methylAggrigation='beta_sum'):
    ''' Load processed meythlation

    :param aggregation_method: beta_corr_tissue, beta_sum
    :return:
    '''
    logging.info('Loading methylation data...')

    if methylAggrigation == 'beta_sum':
       data_file = 'methyl_beta_sum.csv'
    elif methylAggrigation == 'beta_corr_tissue':  # <---------- best
        data_file = 'methyl_gene_beta_corr_tissue.csv'

    methyl_data = pd.read_csv(methyl_processed + data_file,
                             header=0, sep=',|\t', index_col=0, engine='python')

    methyl_data = methyl_data.transpose()
    methyl_data.index.name = 'CELL_LINE'
    methyl_data.index = methyl_data.index.to_series().str.replace('.', '-')
    logging.info('Methylation data loaded, shape: %s', methyl_data.shape)

    methyl_data = methyl_data.astype(float)

    # mismatch name in methyl only
    methyl_data.reset_index(inplace=True)
    methyl_data.replace('X22RV1', '22RV1', inplace=True)
    methyl_data.replace('X647-V', '647-V', inplace=True)
    methyl_data.set_index('CELL_LINE', inplace=True)

    logging.debug(methyl_data.head())

    return methyl_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_cnv_auto(auto_path='auto'):

    logging.info('loading AE cnv data started..')

    cnv_file = os.path.join(processed_data_folder, auto_path, 'cnv_auto.csv')
    logging.info('loading '+ cnv_file)
    cnv_data = pd.read_csv(cnv_file, sep=',', index_col=0, header=0)
    cnv_data.index.name = 'CELL_LINE'
    logging.info( 'cnv_auto shape: %s', cnv_data.shape)
    return cnv_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_cnv_pca(auto_path='pca'):

    logging.info('loading PCA cnv data started..')

    cnv_file = os.path.join(processed_data_folder, auto_path, 'cnv_pca.csv')
    logging.info('loading '+ cnv_file)
    cnv_data = pd.read_csv(cnv_file, sep=',', index_col=0, header=0)
    cnv_data.index.name = 'CELL_LINE'
    logging.info('cnv_PCA shape: %s', cnv_data.shape)
    return cnv_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_cnv(load_processed=True, cnv_filter=None, cnv_aggr=0):

    if cnv_aggr == 'raw':
        cnv_path = os.path.join(processed_data_folder, 'cnv', 'cnv_raw_categorical.tsv')
    elif cnv_aggr == 0:
        cnv_path = os.path.join(processed_data_folder, 'cnv', 'cnv_raw_categorical_less.tsv')
    elif cnv_aggr == 1:
        cnv_path = os.path.join(processed_data_folder, 'cnv', 'cnv_aggregated.tsv')

    logging.info('loading cnv data file:' + cnv_path)

    if load_processed:
        cnv_data = pd.read_csv(cnv_path, sep='\t',header=0, index_col=[0,1])
    else:
        cnv_data = load_cnv_allcelllines(cnv_aggregation=cnv_aggr)
        cnv_data.to_csv(cnv_path, sep='\t')


    cnv_data.index.rename(['CELL_LINE','cnv_prop'], inplace=True)
    cnv_data = cnv_data.unstack(level=-1)

    # Impute minCN and maxCN
    if cnv_aggr in ['old', 'raw', 0]:
        genes = set(cnv_data.columns.get_level_values(0))

        for gene in genes:
            cnv_data[gene, 'minCN'].fillna(np.mean(cnv_data[gene, 'minCN']), inplace=True)
            cnv_data[gene, 'maxCN'].fillna(np.mean(cnv_data[gene, 'maxCN']), inplace=True)

    #impute aggregated feature
    if cnv_aggr == 1:
        genes = set(cnv_data.columns.get_level_values(0))
        for gene in genes:
            cnv_data[gene, 'summary'].fillna(np.mean(cnv_data[gene, 'summary']), inplace=True)


    logging.info('CNV data Loaded, shape: %s', cnv_data.shape)
    logging.debug(cnv_data.head())

    if cnv_filter:
        cnv_data = filterData(cnv_data, cnv_filter, 'CNV')

    return cnv_data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def convert_geneNames_multiIndex(data):
    labels = list(data.columns.levels[0])
    newLabels = [None]*len(labels)
    for i, label in enumerate(labels):
        newLabels[i] = label.split('_')[0]
    data.columns.set_levels(newLabels, level=0, inplace=True)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def add_drug_info(drug):
    infofile = processed_data_folder + '/drugs/drug_info.csv'
    drug_info = pd.read_csv(infofile)

    drug_info.drop('Target(Official Symbol)', axis =1, inplace=True)
    drug_info.drop('SMILES', axis =1, inplace=True)
    drug_info.drop('PubChemID', axis =1, inplace=True)
    drug_info.drop('MW', axis =1, inplace=True)

    logging.info('drug info shape', drug_info.shape)

    drug = pd.merge(drug, drug_info, how='left', left_on='COMPOUND_A', right_on='ChallengeName')
    drug = pd.merge(drug, drug_info, how='left', left_on='COMPOUND_B', right_on='ChallengeName')

    return drug

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_drug(data_phase=phase.training, challenge=1, drug_targets=True,
                processed_targets=True, drug_info= False, drug_auto= None):
    """load combination drug training data
    """

    if data_phase == phase.training:
        logging.info('loading drug data (training phase)')
        file = drug_data_path + 'ch1_train_combination_and_monoTherapy.csv'
    elif data_phase == phase.leaderboard:
        logging.info('loading drug data (leaderboard phase)')
        if challenge == 2:
            file = drug_data_path + 'ch2_leaderBoard_monoTherapy.csv'
        else:
            file = drug_data_path + 'ch1_LB.csv'
    elif data_phase == phase.test:
        logging.info('loading drug data (test phase)')
        if challenge == 2:
            file = drug_data_path + 'ch2_test_monoTherapy.csv'
        else:
            file = drug_data_path + 'ch1_test_monoTherapy.csv'

    data = pd.read_csv(file ,sep=',')
    #convert COMBINATION_ID column to Categorical
    combination_factor = pd.Categorical.from_array(data['COMBINATION_ID'])# default order: alphabetical
    data['combinationFactor'] = combination_factor.codes # insert in dataframe

    data_filtered = data[data.QA ==1].copy()
    data_filtered.drop('QA', axis=1, inplace=True) # drop 'QA' column

    #~~~~~~~~ Scale numeric features
    data_filtered.iloc[:, 3:11] = p.Normalizer().transform(data_filtered.iloc[:, 3:11])

    if (drug_targets):
        moreFeatures = load_moreCombined_drug_features(data_filtered, processed_targets)
        data_filtered = pd.merge(data_filtered, moreFeatures)

    if drug_info:
        data_filtered = add_drug_info(data_filtered)

    if not drug_auto is None:
        drug_info = load_drug_auto(drug_auto)
        drug_info.index.name = 'CELL_LINE'

        drug_info.reset_index(inplace=True)
        data_filtered = pd.merge(data_filtered, drug_info, how='left', on=['CELL_LINE','COMBINATION_ID'] )

    logging.info('Drug data Loaded, shape: %s', data_filtered.shape)
    logging.debug(data_filtered.head())

    data_filtered.set_index('CELL_LINE', inplace=True)
    return data_filtered

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_moreCombined_drug_features(data, processed_targets=True):
    """
        Add more prior knowledge features for drug combinations.

        Features:
        --------
        1- Boolean feature of all Drug targets
        2- shareTF: =1 if both drugs share a TF, 0 otherwise (all are zeros in ch1)
        3- Combined Targets

    :param data: DataFrame with drug training data
    :return: Dataframe with the extra features
    """

    # generate_drug_targets_table()
    if processed_targets:
        logging.info("Loading processed drug targets...")
        targets = pd.read_csv(processed_data_folder+'drugs/drug_target_compact.csv', index_col=0)
    else:
        targets = pd.read_csv(processed_data_folder+'drugs/drug_target.csv', index_col=0)

    combinations = data[['COMBINATION_ID', 'COMPOUND_A', 'COMPOUND_B']].drop_duplicates()

    combinations['shareTF'] = 0

    # Feature 1: shareTF
    # ------------------
    # For all drug combinations that appear in the train data
    for i in combinations.index:
        drugA = combinations.ix[i, 'COMPOUND_A']
        drugB = combinations.ix[i, 'COMPOUND_B']
        score = np.logical_and(targets.ix[drugA], targets.ix[drugB])
        combinations.ix[i, 'shareTF'] = sum(score)


    # Combined Targets of the drugs
    # ------------------
    targetsCol = pd.DataFrame(columns=targets.columns.values)
    combinations = pd.concat([combinations, targetsCol], join='outer')
    for i in combinations.index:
        drugA = combinations.ix[i, 'COMPOUND_A']
        drugB = combinations.ix[i, 'COMPOUND_B']
        score = np.logical_or(targets.ix[drugA], targets.ix[drugB]).astype(int)
        combinations.ix[i, targets.columns.values] = score

    return combinations

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def generate_drug_targets_table(compactTargets=True):
    """
        Generate a 2D table M, with rows are the drugs and columns are the
        unique Targets that appeared as targets in the drug_info file.
        M[d, f] = 1  if Target f is a target of drug d.
        The matrrix M is sparse

        Save the output to file:
            Drug_synergy_data//processed/drug_target.csv

    :return: pandas DataFrame of the matrix M
    """
    if compactTargets:
        inFile = processed_data_folder + 'drugs/targets_names_manual_processing.csv' # manually expanded targets with *
        outFile1 = processed_data_folder + 'drugs/drug_target_compact.csv'
        outFile2 = processed_data_folder + 'drugs/unique_target_compact.csv'
    else:
        inFile =  drug_data_path + 'Drug_info_release.csv'
        outFile1 = processed_data_folder + 'drugs/drug_target.csv'
        outFile2 = processed_data_folder + 'drugs/unique_target.csv'

    drug_info = pd.read_csv(inFile, sep=',', doublequote=False)
    logging.info("Nubmer of Drugs x Number of features: %s", drug_info.shape)

    drugNames = drug_info.ChallengeName.values
    list_of_lists = drug_info['Target(Official Symbol)'].str.replace(' ', '').str.split(',')
    targetsList = [y for x in list_of_lists for y in x]
    targetsList = np.unique(targetsList)
    pd.DataFrame(targetsList).to_csv(outFile2, index=None, header=False)

    logging.info('Number of unique Targets= %s', len(targetsList))
    logging.debug('Unique TFs: %s', targetsList)

    targets = pd.DataFrame(np.zeros([len(drugNames), len(targetsList)]),
                           index=drugNames, columns=targetsList)
    for d, l in zip(drugNames, list_of_lists):
        targets.loc[d, l] = 1
    targets.to_csv(outFile1, sep=',')

    return targets

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_celllinesInfo():
    logging.info('Loading cellline information ...')
    cellline_file = molecular_data_folder + 'cell_info.csv'
    data = pd.read_csv(cellline_file ,sep=',')
    #workaround: the first column is nammed in the gex.csv file
    c =  data.columns.values
    c[0] = 'CELL_LINE'
    data.columns = c

    data.set_index('CELL_LINE',inplace=True)
    logging.info('cellline information loaded: %s', data.shape)

    return data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def process_mutation_all_cellines(aggr_type=0):
    ''' Pprocesses mutations for all cell lines.
        For each cell line, it returns number of features according to the aggre_type
    '''

    celllines_df = load_celllinesInfo()
    celllines = celllines_df.index
    mutations_loaded = pd.read_csv(molecular_data_folder + 'mutations.csv', header=0, sep=',')

    dfs = list()
    logging.info('processing mutation file started ...')
    for thiscellline in celllines:
        mut_thiscellline_df = process_mutation_thiscellline(thiscellline, mutations_loaded,
                                                aggr_type=aggr_type)
        dfs.append(mut_thiscellline_df)
        print('*'),

    concate_df_celllines = pd.concat(dfs, axis=1, keys=celllines)
    concate_df_celllinesT = concate_df_celllines.transpose() # make celllines as rows, genes as columns
    logging.info('\nProcessing mutation file finished for all cell lines. '
                 'Shape of dataframe is: %s', concate_df_celllinesT.shape)

    return concate_df_celllinesT

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def process_mutation_thiscellline(thisCellline, mutation_df, aggr_type=0):
    """
     Extract features for mutations for all genes of one cell line

    :param aggr_type:
                'raw': as many features as possible
                0: best set of features
                1: aggregated features to have one feature only per gene per celline
    """

    gene_dic = {}
    gene_augmentation_dic = {}
    mutation_thisCellline = mutation_df[mutation_df['cell_line_name']== thisCellline]
    featuesNames = ['mutation_zygosity_hom' , "mutation_zygosity_het", "FATHMM_prediction_cancer",
                    "somatic_status_observedOtherCancers", "somatic_status_confirmed",
                    "mut_freq", "mut_descrip", "cds_freq"]

    if aggr_type == 'raw':
        featuesNames.extend(['Gene_CDS_length', 'strand', 'snp'])

    number_features = len(featuesNames)
    for i in mutation_thisCellline.index:
        gene_name = mutation_thisCellline.ix[i,0].strip()
        primary_site = mutation_thisCellline.ix[i,7]
        mutation_zygosity = mutation_thisCellline.ix[i,16] # het, homo
        FATHMM_prediction = mutation_thisCellline.ix[i,21] # PASSENGER/OTHER, CANCER
        somatic_status = str(mutation_thisCellline.ix[i,22]).lower() # Confirmed somatic mutant, Reported in another cancer sample as somatic
        mutation_description = mutation_thisCellline.ix[i,15] # Mutation.Description
        Gene_CDS_length = mutation_thisCellline.ix[i,2]

        # Note: half data has NaN strand, impute it with the mean: 0.5
        strand = 1 if mutation_thisCellline.ix[i,19]=='+' else 0
        cds_freq = get_mut_CDS_freq(mutation_thisCellline.ix[i,13])

        # Note: for NaN SNP value, impute it with 'n'
        if mutation_thisCellline.ix[i,20] =='y':
            snp = 1
        elif mutation_thisCellline.ix[i,20] == 'n':
            snp = 0
        else:
            snp = 0.5


        if somatic_status.find('confirmed') != -1:
            somatic_status_confirmed = 1
        else:
            somatic_status_confirmed = 0

        if somatic_status.find('reported') != -1:
            somatic_status_othercancers = 1
        else:
            somatic_status_othercancers = 0


        if FATHMM_prediction == "CANCER":
            FATHMM_prediction_cancer = 1
        else:
            FATHMM_prediction_cancer = 0

        if mutation_zygosity == 'hom':
            mutation_zygosity_hom = 1
            mutation_zygosity_het = 0
        elif mutation_zygosity == 'het':
            mutation_zygosity_het = 1
            mutation_zygosity_hom = 0
        else:
            mutation_zygosity_hom = 0
            mutation_zygosity_het = 1   # make it 1 (missing data)

        if isinstance(mutation_description, float):  # nan: missing data, use 'unknown
            mutation_description = 'Unknown'

        if 'inframe' in mutation_description or 'In frame' in mutation_description \
            or 'coding silent' in mutation_description:
            mutation_description_val = 0
        elif 'Missense' in mutation_description or 'Nonstop' in mutation_description \
            or 'compound' in mutation_description:
            mutation_description_val = 2
        elif 'Unknown' in mutation_description: # missing data, a lot
            mutation_description_val = 2
        elif 'Nonsense' in mutation_description:
            mutation_description_val = 2
        elif 'Frameshift' in mutation_description or 'frameshift' in mutation_description:
            mutation_description_val = 3


        # ------------------
        if gene_dic.has_key(gene_name):
            gene_dic[gene_name][0] += mutation_zygosity_hom
            gene_dic[gene_name][1] += mutation_zygosity_het
            gene_dic[gene_name][2] += FATHMM_prediction_cancer
            gene_dic[gene_name][3] += somatic_status_othercancers
            gene_dic[gene_name][4] += somatic_status_confirmed
            gene_dic[gene_name][5] += 1 # adding mutation frequency per gene
            gene_dic[gene_name][6] += mutation_description_val
            gene_dic[gene_name][7] += cds_freq

            if aggr_type == 'raw':  # extra features
                gene_dic[gene_name][8] += Gene_CDS_length
                gene_dic[gene_name][9] += strand
                gene_dic[gene_name][10] += snp


        else:
            gene_dic[gene_name] = [0] * number_features
            gene_dic[gene_name][0] = mutation_zygosity_hom
            gene_dic[gene_name][1] = mutation_zygosity_het
            gene_dic[gene_name][2] = FATHMM_prediction_cancer
            gene_dic[gene_name][3] = somatic_status_othercancers
            gene_dic[gene_name][4] = somatic_status_confirmed
            gene_dic[gene_name][5] = 1 # adding mutation frequency per gene
            gene_dic[gene_name][6] = mutation_description_val
            gene_dic[gene_name][7] = cds_freq
            if aggr_type == 'raw':
                gene_dic[gene_name][8] = Gene_CDS_length
                gene_dic[gene_name][9] = strand
                gene_dic[gene_name][10] = snp

    # Feature augmentation into one feature
    if aggr_type == 1:
        for thisgene in gene_dic.keys():
            # homo:   2 * (mut_freq + mutation_description_val)
            # hetro:       mut_freq + mutation_description_val
            aggregated = gene_dic[thisgene][5] + \
                         gene_dic[thisgene][6]
            if gene_dic[thisgene][0] == 1: # homozygous
                aggregated = 2 * aggregated
            gene_augmentation_dic[thisgene] = aggregated
        gene_dic = gene_augmentation_dic
        featuesNames = ['summary']


    df_thiscellline = pd.DataFrame.from_dict(gene_dic, orient='index')
    df_thiscellline.columns = featuesNames

    return df_thiscellline

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def get_mut_CDS_freq(cds):
    """ Frequency of mutations (prior knowledge)
    """
    if isinstance(cds, float):  # Nan
        freq = 3.84     #mean of known values
    elif any(w in cds for w in ['A>C', 'A>T', 'C>A', 'C>G', 'T>A', 'T>G', 'G>T', 'G>C']):
        freq = 2.5
    elif any(w in cds for w in ['C>T', 'A>G', 'T>C', 'G>A']):
        freq = 5
    elif 'del' in cds:
        freq = 0.5968
    elif 'ins' in cds:
        freq = 0.5315
    else:   # other 39 cases of complex mut,  unknown fre
        freq = 3.84

    return freq

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_cnv_data(genes=True, segments=False, type=None):
    '''Extract CNV features for all cell lines
    :param segments: Bool Load segments data file
    :param genes: Bool, load genes data file
    '''

    cnv_segments_df = None
    cnv_genes_df = None
    if segments == True:
        logging.info('loading CNV segments data ...')
        cnv_segments_df = pd.read_csv(molecular_data_folder + 'cnv/cnv_segment.csv', sep=",", header=0)
        c =  cnv_segments_df.columns.values
        c[0] = 'CELL_LINE'
        cnv_segments_df.columns = c
        cnv_segments_df.set_index('CELL_LINE',inplace=True)

    if genes == True: #create a matrix of celllines versus segments
        logging.info('loading CNV genes data ...')
        cnv_genes_df = pd.read_csv(molecular_data_folder + 'cnv/cnv_gene.csv', sep=",", header=0)


    return cnv_genes_df, cnv_segments_df

# -------------------------------------------------------------------------

def load_cnv_thiscellline(cnv_genes_df, thiscellline, cnv_aggregation=0):
    '''
        extract features for a single cell line for all its genes
    '''
    if cnv_aggregation == 'raw':
        columnsNames = ['maxCN','minCN', 'zygosity_H', 'zygosity_L',
                               'zygosity_0', 'disruption']
    elif cnv_aggregation == 0:
        columnsNames = ['maxCN','minCN', 'zygosity', 'disruption']
    elif cnv_aggregation == 1:
        columnsNames = ['summary']

    cnv_genes_thiscellline = cnv_genes_df[cnv_genes_df['cell_line_name'] == thiscellline]
    cnv_genes_thiscellline = cnv_genes_thiscellline[cnv_genes_thiscellline['chr_GRCh37']!= 'Y'] #igonore sex bias, as recommanded in the description of the dat
    genes_dic = {}
    for i in cnv_genes_thiscellline.index:
        gene_id = cnv_genes_thiscellline.ix[i]['gene']
        maxCN = cnv_genes_thiscellline.ix[i]['max_cn_GRCh37']
        minCN = cnv_genes_thiscellline.ix[i]['min_cn_GRCh37']
        zygosity = cnv_genes_thiscellline.ix[i]['zygosity_GRCh37']
        disruption = cnv_genes_thiscellline.ix[i]['disruption_status_GRCh37']

        if minCN < 0 and maxCN < 0: # missing data row
            continue

        if np.isnan(minCN) or np.isnan(maxCN):
            continue

        if zygosity == 'L':
            zygosity_numeric = 2
        elif zygosity == 'H':
            zygosity_numeric = 1
        elif zygosity == '0':
            zygosity_numeric = 0

        features = [0] * len(columnsNames)

        if cnv_aggregation == 1:    # aggregation
            if disruption == 'D':
                features[0] = np.nan    # consider unknown
            else:
                features[0] = zygosity_numeric * maxCN
        else:
            features[0:2] = maxCN, minCN
            features[2] = (1 if disruption == 'D' else 0) # '-' mapped to 0, no missing data

            if cnv_aggregation == 'raw':
                if zygosity == 'H':
                    features[3:6] = [1, 0, 0]
                elif zygosity == 'L':
                    features[3:6] = [0, 1, 0]
                elif zygosity == '0':
                    features[3:6] = [0, 0, 1]
                else:
                    print cnv_genes_thiscellline.ix[i]
                    print 'missing zygosity!!!'
            elif cnv_aggregation == 0:
                features[3] = zygosity_numeric


        genes_dic[gene_id] = features

    df_thiscellline = pd.DataFrame.from_dict(genes_dic, orient='index')
    df_thiscellline.index.name = 'AGI'

    df_thiscellline.columns = columnsNames


    return df_thiscellline

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_cnv_allcelllines(cnv_aggregation=0):
    celllines_df = load_celllinesInfo()
    celllines = celllines_df.index

    cnv_genes, cnv_segments = load_cnv_data(segments=False, genes=True, type=cnv_aggregation)
    dfs = list()
    logging.info('processing each cellline has started. This may take a while ...')
    for thiscellline in celllines:
        cur_cellline_cnv_df = load_cnv_thiscellline(cnv_genes, thiscellline, cnv_aggregation)
        dfs.append(cur_cellline_cnv_df)
        print('*'),
    concate_df_celllines = pd.concat(dfs, axis=1, keys=celllines)
    concate_df_celllinesT = concate_df_celllines.transpose() #make celllines as rows, genes as columns
    logging.info('processing all celllines has finished ...')
    logging.info('output shape: %s', concate_df_celllinesT.shape)

    return concate_df_celllinesT

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def load_tissues():
    logging.info('Loading tissue information ...')
    cellline_file = molecular_data_folder + 'cell_info.csv'
    data = pd.read_csv(cellline_file , sep=',')
    #workaround: the first column is nammed in the gex.csv file
    c =  data.columns.values
    c[0] = 'CELL_LINE'
    data.columns = c

    data.set_index('CELL_LINE',inplace=True)
    logging.info('cellline information loaded: %s', data.shape)

    return data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def filterData(data, filterGenesFile, dataType):
    '''
    :param data: DataFrame with genenames as columns level 0
    :param filterGenesFile: File path to the filter file
    :param dataType: string of datatype for logging
    :return: filtered data using genes only in the filterGenesFile
    '''

    logging.info('Applying special filter on '+dataType)
    filterGenes = pd.read_csv(os.path.join(processed_data_folder,filterGenesFile))
    filterGenes = filterGenes.astype(unicode).iloc[:,0].tolist() # convert to unicode to match multi indexes
    logging.info('Number of genes in '+dataType+' filter: %s', len(filterGenes))
    genes = data.columns.get_level_values(0)

    common_genes = []
    for geneName in genes:
        symbol = geneName.split('_')[0]
        if (symbol in filterGenes):
            common_genes.append(geneName)

    data = data[np.unique(common_genes)]
    logging.info('Filtered '+dataType+' shape is: %s', data.shape)

    return data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
