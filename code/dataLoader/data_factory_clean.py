__author__ = 'doaa'

import pandas as pd
import numpy as np
from dataLoader import data_low_level_clean as d
import logging, os
from dataLoader.data_low_level_clean import config

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# cashed molecular data with different params
# identified by a hashing of the params string
cashed_data = {}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def get_drug(params, data_phase = d.phase.training):

    drug_params = {'drug_targets': params['targets'], 'challenge': params['challenge'],
                       'processed_targets': params['processed_targets']}


    if 'drug_info' in params:
        drug_params['drug_info'] = params['drug_info']

    params_hash = hash(str(drug_params))

    if data_phase == d.phase.training and params_hash in cashed_data:
        return cashed_data[params_hash].copy()   # return cashed


    data = d.load_drug(data_phase = data_phase, **drug_params)

    # Remove monotherapy and combination factor for subchallenge B
    # Remove: IC50_A, H_A, Einf_A, IC50_B, H_B, Einf_B
    if params['subchallenge'] == 'B':
        data.drop(data.columns.values[4:10], inplace=True, axis=1)
        data.__delitem__('combinationFactor')


    # For subchallenge A, optional to remove monotherapy
    if 'drug_features' in params:
        if not params['drug_features']: # 6 features of the 2 drugs
            data.drop(data.columns.values[4:10], inplace=True, axis=1)


    # For subchallenge A, optional to remove combination factors
    if params['subchallenge'] == 'A':
       if not params['combination_factor']:
           data.__delitem__('combinationFactor') # fix bug ( drop does not work)
       else:
           id = pd.get_dummies(data['COMBINATION_ID'])
           cell = pd.get_dummies(data.index)
           cell.index= data.index
           data = pd.concat([data, cell, id], ignore_index= False, axis=1)
           data.__delitem__('combinationFactor')


    # cashing
    if data_phase == d.phase.training:
        cashed_data[params_hash] = data.copy()
    return data


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def get_data(params, multiindex=False):
    """
    This is the main function to get the processed data.
    Use params to choose what kind of data to load.

    :return: DataFrame of the whole data, rows are the cell lines
                    columns are the genes or subset of the genes
    """

    global cashed_data

    original_data, autoEncoder_data, pca_data = [], [], []
    original_keys, auto_keys, pca_keys = [], [], []

    auto_path = params['auto_path']
    pca_path = params['pca_path']

    #~~~~~~~~~~~~~~~~ loading gene expression
    if params['load_ge']:
        ge_params = {'ge_impute': params['ge_impute']}
        params_hash = hash(str(ge_params))
        logging.info('drugs params: %s', str(ge_params))
        if params_hash in cashed_data:
            ge_data = cashed_data[params_hash].copy()
        else:
            ge_data = d.load_ge(**ge_params)
            cashed_data[params_hash] = ge_data.copy()

        original_data.append(ge_data)
        original_keys.append(('ge'))

    #~~~~~~~~~~~~~~~~ loading AE gene expression
    if params['ge_auto']==True:
        logging.info('loading AE gene expression data')
        ge_data = d.load_ge_auto(auto_path)
        autoEncoder_data.append(ge_data)
        auto_keys.append(('ge_AE'))

    #~~~~~~~~~~~~~~~~ loading PCA gene expression

    if params['ge_pca']==True:
        logging.info('loading PCA gene expression data')
        ge_data = d.load_ge_pca(pca_path)
        pca_data.append(ge_data)
        pca_keys.append(('ge_PCA'))


    #~~~~~~~~~~~ loading mutation ~~~~~~~~~~~
    if params['load_mut']:
        mu_params = {'mut_agg_type' : params['mut_agg_type'],
                     'mut_filter': params['mut_filter']}
        params_hash = hash(str(mu_params))
        if params_hash in cashed_data:
            mut_data = cashed_data[params_hash].copy()
        else:
            mut_data = d.load_mutation(**mu_params )
            cashed_data[params_hash] = mut_data.copy()

        original_data.append(mut_data)
        original_keys.append(('mu'))

    #~~~~~~~~~~~ loading AE mutation ~~~~~~~~~~~
    if params['mut_auto']==True:
        logging.info('loading AE mutation data')
        mut_data = d.load_mutation_auto(auto_path)
        autoEncoder_data.append(mut_data)
        auto_keys.append(('mut_AE'))

    #~~~~~~~~~~~ loading PCA mutation ~~~~~~~~~~~
    if params['mut_pca']==True:
        logging.info('loading PCA mutation data')
        mut_data = d.load_mutation_pca(pca_path)
        pca_data.append(mut_data)
        pca_keys.append(('mut_PCA'))


    #~~~~~~~~~~~~~~~ loading cnv ~~~~~~~~~~~~~
    if params['load_cnv']:
        cnv_params = {'cnv_aggr': params['cnv_agg_type'],
                      'cnv_filter': params['cnv_filter']}
        params_hash = hash(str(cnv_params))
        if params_hash in cashed_data:
            cnv_data = cashed_data[params_hash].copy()
        else:
            cnv_data = d.load_cnv(**cnv_params)
            cashed_data[params_hash] = cnv_data.copy()
        original_data.append(cnv_data)
        original_keys.append(('cnv'))

    #~~~~~~~~~~~~~~~ loading AE cnv ~~~~~~~~~~~~~
    if params['cnv_auto'] == True:
        logging.info('loading AE CNV data')
        cnv_data = d.load_cnv_auto(auto_path)
        autoEncoder_data.append(cnv_data)
        auto_keys.append(('cnv_AE'))

        #~~~~~~~~~~~~~~~ loading PCA cnv ~~~~~~~~~~~~~
    if params['cnv_pca'] == True:
        logging.info('loading PCA CNV data')
        cnv_data = d.load_cnv_pca(pca_path)
        pca_data.append(cnv_data)
        pca_keys.append(('cnv_PCA'))


    #~~~~~~~~~~~~~~~~ loading methylation ~~~~~~~~~~
    if params['load_meyth']:
        meth_params = {'methylAggrigation' : params['meyth_aggr']}
        params_hash = hash(str(meth_params))
        if params_hash in cashed_data:
            methylation_data = cashed_data[params_hash].copy()
        else:
            methylation_data = d.load_methylation(**meth_params)
            cashed_data[params_hash] = methylation_data.copy()

        original_data.append(methylation_data)
        original_keys.append(('meth'))

    #~~~~~~~~~~~~~~~~ loading AE methylation ~~~~~~~~~~
    if params['meth_auto'] == True:
        print 'loading AE methylation data'
        methylation_data = d.load_methylation_auto(auto_path)
        autoEncoder_data.append(methylation_data)
        auto_keys.append(('meth_AE'))

    #~~~~~~~~~~~~~~~~ loading PCA methylation ~~~~~~~~~~
    if params['meth_pca'] == True:
        print 'loading PCA methylation data'
        methylation_data = d.load_methylation_pca(pca_path)
        pca_data.append(methylation_data)
        pca_keys.append(('meth_pca'))


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #~~~~~~~~~~~~~~  Pathways filtering (original data only) ~~~~~~~
    pathways = params['pathways']
    if pathways is not None:
        pathwayGenes = getPathwaygenes_(pathways)
        logging.info('Number of genes in Pathways: %s', pathwayGenes.shape)
        if params['prior_integration'] == 'filtering':
            for i, ds in enumerate(original_data):
                logging.info('Filtering genes for data type: %s', original_keys[i])
                original_data[i] = filter_selected_data_complexNames(ds, pathwayGenes)


    #~~~~~~~~~~~~~~~~~~  Concat data ~~~~~~~~~~~~~~
    logging.info("Concatinating data frames.....")

    # Merge original and AE data and PCA
    original_data = original_data + autoEncoder_data + pca_data
    keys = original_keys + auto_keys + pca_keys

    #~~~~~~~~~~  Add tissue type
    if 'add_tissues' in params and params['add_tissues'] == True:
        if 'tissue_type' in cashed_data:
            cancerType = cashed_data['tissue_type'].copy()
        else:
            celline_features = pd.read_csv(config['data']['molecular']+'/cell_info.csv', index_col=0)
            cancerType = pd.get_dummies(celline_features['Tissue..General.']) # from categorical to numeric
            cancerType.index.name = 'CELL_LINE'
            cashed_data['tissue_type'] = cancerType.copy()
        original_data.append(cancerType)
        keys.append('tissue_type')

    if original_data:
        if multiindex:
            original_data = repeat_column_index(original_data)
            data = pd.concat(original_data, axis=1, join='outer', keys=keys)
        else:
            data = pd.concat(original_data, axis=1, join='outer')

        logging.info("Shape of final concatinated molecular data is: %s", data.shape)
        logging.debug("Data:")
        logging.debug(data.head())
        logging.info("Replacing NaN with 0....")
        data.fillna(0, inplace=True)
        data.index.name = 'CELL_LINE'
        logging.info("Loading molecular data is done.")
    else:
        data = None
        logging.info("NO data was loaded.")

    return data

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def repeat_column_index(tables):
    max_level = 2
    for t in tables:
        if not isinstance(t.columns, pd.core.index.MultiIndex):
            l = [t.columns.values] * max_level # repeat columns
            t.columns = pd.MultiIndex.from_arrays(l)
            t.index.name = 'CELL_LINE'
    return tables

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def getPathwaygenes_(pathways):
    '''returns the list of genes in the given set of pathway names.
    '''
    genesList = list()
    logging.debug('Loading pathways:  %s', pathways)
    for pathway in pathways:
        list_pathways_df = pd.read_csv(config['pathways'][pathway], sep='\t', header=0)
        genes = list_pathways_df['Symbol'].values
        genesList.append(genes)


    genesList = np.unique(np.concatenate(genesList))

    return genesList

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def filter_selected_data_complexNames(data, pathwayGenes):
    '''filer all genes out except genes in the given pathwayGenes
    '''
    # convert to unicode to match multi indexes
    pathwayGenes_unicode = pathwayGenes.astype(unicode)
    genes = data.columns.get_level_values(0)
    logging.info("Shape of genes: %s", genes.shape)

    common_genes = []
    for geneName in genes:
        symbol = geneName.split('_')[0]
        if (symbol in pathwayGenes_unicode):
            common_genes.append(geneName)

    logging.debug("Interection shape: %s", len(common_genes))
    logging.debug("Number of unique common_genes: %s", np.unique(common_genes).shape)

    missing = np.setdiff1d(pathwayGenes_unicode, common_genes)
    logging.info("Genes in the pathways but NOT in the data: %s", len(missing))
    logging.debug(missing)
    filteredData = data[np.unique(common_genes)]
    logging.info('Filtered data shape is: %s', filteredData.shape)

    return filteredData
