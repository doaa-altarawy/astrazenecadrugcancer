__author__ = 'doaa'

import logging
import os
import pandas as pd
import subprocess
from dataLoader.data_low_level_clean import config

Rscript_path = config['Rscript_path']

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def generate_combin_priority():

    # Generate ranking file:
    logging.info('Generating ranking file..')
    if not os.path.exists("../../results/"):
        os.makedirs("../../results/")
    path = os.path.dirname(os.path.realpath(__file__))
    process = subprocess.Popen ([Rscript_path,'../Rcode/drug_rank.R'],cwd=path,  stdout=subprocess.PIPE , stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print stdout, stderr

    # Remove extra two combinations that causes submission error
    rank = pd.read_csv('../../results/combination_priority.csv')
    rank = rank[rank.COMBINATION_ID != 'BCL2_2.EGFR_2']
    rank = rank[rank.COMBINATION_ID != 'PIK3CB_PIK3CD.PIK3CB_PIK3CD']
    rank.to_csv('../../results/combination_priority.csv', index=None)