__author__ = 'doaa'

import numpy as np
import datetime, sys, os
from dataLoader import data_low_level_clean as d
import logging
from dataLoader.download_data import download_challenge_zipfiles, download_challenge_data, LB_syn_id
from autoencoder import run_autoencoder_clean as AE
from pca import run_pca as PCA
import subprocess
from dataLoader.data_low_level_clean import config

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# setup logging
# Create logs directory
if not os.path.exists('../logs'):
    os.makedirs('../logs')


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# cashed molecular data with different params
# identified by a hashing of the params string
cashed_data = {}
Rscript_path = config['Rscript_path']

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def create_processed_data_directory(path):
    if not os.path.exists('../../processed_data/'+path):
        os.makedirs('../../processed_data/'+path)

def run_R_file(filePath):
    path = os.path.dirname(os.path.realpath(__file__))
    process = subprocess.Popen ([Rscript_path,filePath],cwd=path,  stdout=subprocess.PIPE , stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    print stdout, stderr

# -----------------------------------------------------------------------------

def download_synapse_data():
    # Download and unzip Challenge data (requires login)
    if not os.path.exists('../../data'):
        os.makedirs('../../data')
    download_challenge_zipfiles()
    # download LB training file (missing in Synapse zip download!)
    download_challenge_data(entity_id=LB_syn_id, path='../../data/Drug_synergy_data/')


    # Process drug data
    create_processed_data_directory('drugs')
    d.generate_drug_targets_table(compactTargets=False)
    d.generate_drug_targets_table(compactTargets=True)



# -----------------------------------------------------------------------------

def download_process_data():
    '''
        Run onces to generate processed data, takes around 2 hrs time
    '''
    download_synapse_data()

    # Process Methylation
    create_processed_data_directory('methyl')
    # logging.info('Processing methyl (beta_sum), it may take around 10 mins......')
    # run_R_file('../Rcode/met_beta_sum.R')
    # logging.info('Processing methyl (beta_sum) is done')
    logging.info('Processing methyl correlation with gene expression, it may take around 40 mins......')
    run_R_file('../Rcode/gex_met_corr.R')
    logging.info('Processing methyl correlation with gene expression is done.')


    # Impute gene expression
    logging.info('Imputing gene expression data.')
    create_processed_data_directory('ge')
    run_R_file('../Rcode/ge_impute.R')

    # Process Mutations
    create_processed_data_directory('mut')
    logging.info('Processing mutations..')
    # d.load_mutation(load_processed=False, mut_agg_type=0)
    d.load_mutation(load_processed=False, mut_agg_type=1)


    # Process CNV
    create_processed_data_directory('cnv')
    logging.info('Processing CNV..')
    d.load_cnv(load_processed=False, cnv_aggr=0)
    # d.load_cnv(load_processed=False, cnv_aggr=1)


    # Generate different types of autoencoders for the molecular data
    logging.info('Creating Autoencoders..')
    create_processed_data_directory('auto/no_filter')
    create_processed_data_directory('auto/full_drug_pathways')
    dataTypes = np.array([ 'load_mut', 'load_cnv', 'load_meyth', 'load_ge'])

    # 1- drugs pathways filtering
    # AE.generate_autoencoder(AE.data_params_full_drug_pathways, dataTypes,
    #                         base_directory='../../processed_data/auto/full_drug_pathways/')


    # 2- no filtering, all genes
    AE.generate_autoencoder(AE.data_params_no_filter, dataTypes,
                            base_directory='../../processed_data/auto/no_filter/')

    # Generate PCA for each data type
    PCA.generate_PCA(PCA.data_params, PCA.dataTypes,
                     base_directory='../../processed_data/pca/no_filter/')

# ------------------------------------------------------------

if __name__=='__main__':
    # Set up logging
    base_folder ='../logs'
    timeStamp = '_{0:%b}-{0:%d}_{0:%H}-{0:%M}'.format(datetime.datetime.now())
    filename= os.path.basename(__file__)
    filename, file_extension = os.path.splitext(filename)
    filename = os.path.join(base_folder,filename+ timeStamp+'.log')
    logging.basicConfig(filename = filename,
                        filemode='w',
                        format='%(asctime)s - {%(filename)s:%(lineno)d} - %(message)s',
                        datefmt='%m/%d %I:%M',
                        level=logging.INFO) # or logging.DEBUG
    logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

    download_process_data()

