import cPickle

import sys

import time
from keras.regularizers import l1, l2

__author__ = 'marakeby'
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, ActivityRegularization, AutoEncoder
from keras.optimizers import SGD, RMSprop
import numpy as np

def get_autoencoder(input_shape, output_reconstruction =False, n_hid=100):
    # activation_fn = 'sigmoid'
    activation_fn = 'tanh'
    # activation_fn = 'linear'
    # encoder = Sequential([Dense(n_hid, input_dim=input_shape, activation= activation_fn), Dense(n_hid, activation=activation_fn)])
    # decoder = Sequential([Dense(n_hid, input_dim=n_hid,  activation=activation_fn), Dense(input_shape, activation='linear')])
    init =  'glorot_uniform'
    # encoder = Sequential([Dense(n_hid, input_dim=input_shape, init= init), Dense(n_hid/2, init= init)])
    # decoder = Sequential([Dense(n_hid, input_dim=n_hid/2, init= init), Dense(input_shape, init= init)])

    encoder = Sequential([Dense(n_hid, input_dim=input_shape, activation= activation_fn, W_regularizer=l1(0.00001)), Dense(n_hid, activation='linear', W_regularizer=l2(0.00001))])
    # encoder = Sequential([Dense(n_hid, input_dim=input_shape, activation= activation_fn, init= init, W_regularizer=l1(0.01)), Dense(n_hid, activation='linear', W_regularizer=l2(0.01),  init= init)])
    decoder = Sequential([Dense(n_hid, input_dim=n_hid, activation='linear'), Dense(input_shape,  activation='linear')])


    # encoder = Sequential([Dense(100, input_dim=input_shape, activation= activation_fn)])
    # decoder = Sequential([Dense(input_shape, input_dim=100,  activation='linear')])

    auto = AutoEncoder(encoder=encoder, decoder=decoder, output_reconstruction=output_reconstruction)
    return auto

# def feature_extraction(X_train, X_test):
#     X_train2= np.copy(X_train)
#     model = Sequential()
#     auto = get_autoencoder(200)
#     model.add(auto)
#     sgd = SGD(lr=0.000001, decay=1e-6, momentum=0.9, nesterov=False)
#     model.compile(loss='mean_squared_error', optimizer=sgd)
#     model.fit(X_train2, X_train2,nb_epoch=100, batch_size=10, verbose=1 )
#     xtrain = model.predict(X_train2)
#     xtest = model.predict(X_test)
#     return xtrain, xtest

class Auto(object):
    def __init__(self,n_hid=100, lr= 0.05, epoch=1, batch_size= 50,momentum=.9,  decay=1e-6, output_reconstruction =False):
        self.lr = lr
        self.epoch= epoch
        # self.epoch= 10
        self.batch_size = batch_size
        self.momentum =momentum
        self.decay = decay
        self.output_reconstruction= output_reconstruction
        self.n_hid = n_hid


    def fit(self, X_train):
        n = X_train.shape[1]
        model = Sequential()
        auto = get_autoencoder(n, n_hid =self.n_hid,  output_reconstruction= self.output_reconstruction)
        model.add(auto)
        sgd = SGD(lr=self.lr, decay= self.decay, momentum=self.momentum, nesterov=False)
        model.compile(loss='mean_squared_error', optimizer=sgd)
        # model.compile(loss='mean_squared_error', optimizer=RMSprop())
        model.fit(X_train, X_train,nb_epoch=self.epoch, batch_size=self.batch_size, verbose=1 )
        self.model = model

    def transform(self, X):
         return self.model.predict(X)

    def fit_transform(self, X):
        self.fit(X)
        return self.transform(X)

    def save(self, file_name):
        f = file(file_name+'.pkl', 'wb')
        # sys.setrecursionlimit(100000)
        cPickle.dump(self, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    @staticmethod
    def load( file_name):
        f = file(file_name+'.pkl', 'rb')
        # theano.config.reoptimize_unpickled_function = False
        start = time.time()
        model = cPickle.load(f)
        end = time.time()
        elapsed_time = end - start
        return  model
