import os

from sklearn import preprocessing as p

from dataLoader.data_factory_clean import get_data
from autoencoder import Auto
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

autoencoder_params = {'lr': 0.1,
                      'epoch': 400,
                      'batch_size': 50 , # 10 for ge, meth
                      'momentum':.9,
                      'decay':1e-6,
                      'n_hid' : 500, # number of hidden nodes (number of features needed)
                      'output_reconstruction': False # True: reconstruct the data again, False: return features
                      }

data_params_no_filter = {
                'subchallenge': 'B',
                'load_meyth': False,
                'meyth_aggr': 'beta_corr_tissue', #'beta_sum',
                'load_ge':  False,
                'load_cnv': False,
                'load_mut': False,
                'ge_impute': True,
                'ge_auto': False,
                'mut_auto': False,
                'cnv_auto': False,
                'meth_auto': False,
                'ge_pca': False,
                'meth_pca': False,
                'mut_pca': False,
                'cnv_pca': False,
                'mut_agg_type': 1,
                'cnv_agg_type': 0,
                'mut_filter': None,
                'cnv_filter': None,
                'auto_path': None,
                'pca_path': None,
                'prior_integration': None,
                'scaling_factor':100,
                'pathways': None
                         }

data_params_full_drug_pathways= {
                'subchallenge': 'B',
                'load_meyth': False,
                'meyth_aggr': 'beta_corr_tissue',
                'load_ge':  False,
                'load_cnv': False,
                'load_mut': False,
                'ge_impute': True,
                'ge_auto': False,
                'mut_auto': False,
                'cnv_auto': False,
                'meth_auto': False,
                'ge_pca': False,
                'meth_pca': False,
                'mut_pca': False,
                'cnv_pca': False,
                'mut_agg_type': 1,
                'cnv_agg_type': 0,
                'mut_filter': None,
                'cnv_filter': None,
                'auto_path': None,
                'pca_path': None,
                'prior_integration': 'filtering',
                'scaling_factor':100,
                'pathways': [
                               'pathways_cancer',
                               'minoacyl-tRNA',
                               'NF-kappaB',
                               'cell_cycle',
                               'transforming_growth_factor-beta',
                               'apoptosis',
                               'p53_signaling',
                               'MAPK_signaling',
                               'PI3K-Akt_signaling',
                               'Wnt_signaling',
                               'mTOR_signaling',
                               # 'misreulation_cancer' ##
                             ]
                         }


def normalize(in_data):
    proc = p.StandardScaler()
    # print in_data
    in_data = proc.fit_transform(in_data)
    proc= p.MinMaxScaler(feature_range=(-1, 1),)
    in_data = proc.fit_transform(in_data)
    return in_data

def visualize(orig_data, transformed_data):
    m = np.mean(orig_data, axis=0)

    print orig_data.shape, type(orig_data)
    print transformed_data.shape
    plt.plot(m[0:100])

    m = np.mean(transformed_data, axis=0)
    plt.plot(m[0:100])
    plt.legend(['original','transformed'])
    plt.show()


# # no filtering-----------------
data_params  = data_params_no_filter
# data_params  = data_params_full_drug_pathways
prefix = 'test1'
dataTypes = np.array([ 'load_mut'])
#dataTypes = np.array(['load_meyth', 'load_ge', 'load_cnv', 'load_mut'])
#dataTypes = np.array(['load_meyth'])

# base_directory = '../../processed_data/auto/full_drug_pathways/'
base_directory = '../../processed_data/auto/no_filter/'

random_seed= 8082008
np.random.seed(random_seed)

print('random seed')
print(random_seed)

def generate_autoencoder(data_params=data_params, dataTypes=dataTypes, base_directory=base_directory):
    for i, data_type in enumerate(dataTypes):
        data_params [data_type] = True
        data = get_data(data_params)
        data_params [data_type] = False   # Make it False for next loop
        data.fillna(0, inplace=True)

        info = data.index
        print data.shape
        data = data.as_matrix()

        print("Generating Autoencoder for Data Type:", data_type)
        print data.shape

        data = normalize(data)
        # visualize(data, data)
        # extract features
        if data_type in ['load_meyth', 'load_ge']:
            autoencoder_params['batch_size'] = 10
        else:
            autoencoder_params['batch_size'] = 50

        auto = Auto(**autoencoder_params)
        auto.fit(data)
        data_transformed = auto.transform(data)


        # Save extracted features/ constructed data
        n= data_transformed.shape[1]
        df = pd.DataFrame(data_transformed, index=info, columns=range(0,n))

        save_dir = os.path.join(base_directory, prefix)
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        file_name = data_type
        if data_type == 'load_meyth':
            file_name = 'meth_auto.csv'

        elif data_type == 'load_ge':
            file_name = 'ge_auto.csv'

        elif data_type == 'load_mut':
            file_name = 'mu_auto.csv'

        elif data_type == 'load_cnv':
            file_name = 'cnv_auto.csv'

        file_name = os.path.join(save_dir, file_name)
        df.to_csv(file_name)

generate_autoencoder()
