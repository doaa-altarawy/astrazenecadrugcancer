import pandas as pd

import dataLoader.dataLowLevel as d

# import sklearn

lr = 0.05
epoch= 200
# self.epoch= 10
patch_size= 50
momentum =.90
decay = 1e-6
output_reconstruction = False

params = {'lr': lr, 'epoch': epoch, 'patch_size':patch_size ,'momentum':momentum,  'decay':decay, 'output_reconstruction': output_reconstruction}

from sklearn import preprocessing as p
ge  = d.load_mutation(verbose = True)
from autoencoder import Auto
import numpy as np
from matplotlib import pyplot as plt
ge.fillna(0, inplace=True)
info = ge.index
ge = ge._get_numeric_data().as_matrix()
proc =  p.StandardScaler()
ge = proc.fit_transform(ge)
proc= p.MinMaxScaler(feature_range=(-1, 1),)
ge = proc.fit_transform(ge)

m = np.mean(ge,axis=0)

print ge.shape, type(ge)
auto = Auto(**params)
auto.fit(ge)
ge2 = auto.transform(ge)

print ge2.shape

# err = mean_squared_error(ge, ge2)
# print 'error', err


plt.plot(m[0:100])
# plt.plot(m)
# plt.show()
# ge= p.normalize(ge, norm='l2')
m = np.mean(ge2,axis=0)
plt.plot(m[0:100])
plt.legend(['original','transformed'])
plt.show()

n= ge2.shape[1]
df = pd.DataFrame(ge2, index=info, columns=range(0,n))
df.to_csv('../../data/Sanger_molecular_data/mu_auto.csv')
