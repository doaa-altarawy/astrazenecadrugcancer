# import sklearn
import os

from sklearn import preprocessing as p

from dataLoader.data_factory import get_data, get_drug
from autoencoder import Auto
import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

from expr.train_predict import prepare_inputs

autoencoder_params = {'lr': 0.05,
                      'epoch': 200,
                      'batch_size':50 , # 10 for ge, meth
                      'momentum':.9,
                      'decay':1e-6,
                      'n_hid' : 500, # number of hidden nodes (number of features needed)
                      'output_reconstruction': False, # True: reconstruct the data again, False: return features

                      }

data_params =  {
                'drug_features': {
                            'targets': True,
                            'drug_features': True,
                            'combination_factor':False,
                            'processed_targets': True,
                            'drug_info': True,
                            'log_scores': False,
                            'challenge':1,
                    'drug_auto' : None
                        },
                'load_meyth': False, 'meth_auto' : False,
                'meyth_aggr':  'beta_corr_tissue',
                'load_ge':  False, 'ge_auto':False,
                'ge_impute': True,
                'load_cnv': False, 'cnv_auto':False,
                'load_mut': False, 'mut_auto':False,
                'mut_agg_type': 1,
                'prior_integration': None,
                'scaling_factor':100,
                'pathways': None
}

def normalize(in_data):
    proc =  p.StandardScaler()
    in_data = proc.fit_transform(in_data)
    proc= p.MinMaxScaler(feature_range=(-1, 1),)
    in_data = proc.fit_transform(in_data)
    return in_data

def visualize(orig_data, transformed_data):
    m = np.mean(orig_data, axis=0)

    print orig_data.shape, type(orig_data)
    print transformed_data.shape
    # plt.plot(m[0:100])
    plt.plot(m)

    m = np.mean(transformed_data, axis=0)
    # plt.plot(m[0:100])
    plt.plot(m)
    plt.legend(['original','transformed'])
    plt.show()


# # no filtering-----------------
prefix = 'no_filter_500'



base_directory = '../../processed_data/auto'

data = get_drug(data_params['drug_features'])
# data,y, info = prepare_inputs(data_params)
# data.fillna(0, inplace=True)

# info = data.index
print 'data.shape', data.shape
# print data.head()
# data = normalize(data)
# data = data.as_matrix()



print data.shape, type(data)
data.reset_index(inplace=True)
# print info
info = data.loc[:, [ 'CELL_LINE', 'COMBINATION_ID']]
print info.head()
data.fillna(0, inplace=True)
data.__delitem__('SYNERGY_SCORE')
data = data.convert_objects(convert_numeric=True)
print("Data + Drug: %s", data.head())
data = data._get_numeric_data()

data= data.as_matrix()

print 'mean', np.mean(data)
# visualize(data, data)
# extract features
auto = Auto(**autoencoder_params)
auto.fit(data)
data_transformed = auto.transform(data)
print 'data_transformed', data_transformed.shape
# visualize(data, data_transformed)

# Save extracted features/ constructed data
n= data_transformed.shape[1]
ind = info.loc[:,'CELL_LINE'].as_matrix()
print ind.shape

# print
df = pd.DataFrame(data_transformed,  columns=range(0,n))
df = pd.concat([info, df], axis=1)
save_dir = os.path.join(base_directory,prefix )
if not os.path.exists(save_dir):
    os.makedirs(save_dir)

file_name = 'drug_auto.csv'

# file_name = file_name + '.csv'
file_name = os.path.join(save_dir, file_name )
df.to_csv( file_name, index= False)
#save model
auto.save(file_name)