import pandas as pd

import dataLoader.dataLowLevel as d
from sklearn import preprocessing as p
from autoencoder import Auto
import numpy as np
from matplotlib import pyplot as plt

# import sklearn
def get_data():
    ge  = d.load_ge(verbose = True, ge_auto=False)
    ge.fillna(0, inplace=True)
    info = ge.index
    ge = ge._get_numeric_data().as_matrix()
    return ge, info

def normalize(ge):
    proc =  p.StandardScaler()
    ge = proc.fit_transform(ge)
    proc= p.MinMaxScaler(feature_range=(-1, 1),)
    ge = proc.fit_transform(ge)
    return ge

def visualize(ge, ge2):
    m = np.mean(ge,axis=0)

    print ge.shape, type(ge)
    print ge2.shape
    plt.plot(m[0:100])

    m = np.mean(ge2,axis=0)
    plt.plot(m[0:100])
    plt.legend(['original','transformed'])
    plt.show()

lr = 0.05
epoch= 200
patch_size= 50
momentum =.90
decay = 1e-6
output_reconstruction = False

params = {'lr': lr, 'epoch': epoch, 'patch_size':patch_size ,'momentum':momentum,  'decay':decay, 'output_reconstruction': output_reconstruction}

# get data
ge, info = get_data()
ge = normalize(ge)

# extract features
auto = Auto(**params)
auto.fit(ge)
ge2 = auto.transform(ge)

auto.save('ge_auto')

# auto2 = Auto.load('ge_auto')
# auto2.model.output_reconstruction =False
# ge3= auto2.model.get_output(ge)

# ge3= auto2.transform(ge)

# print ge3.shape

# visualize(ge2, ge3)

# err = mean_squared_error(ge, ge2)
# print 'error', err

# Save
# n= ge2.shape[1]
# df = pd.DataFrame(ge2, index=info, columns=range(0,n))
# df.to_csv('../../data/Sanger_molecular_data/ge_auto.csv')
